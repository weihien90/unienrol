<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * Return the More Articles example
     *
     * @return \Illuminate\Http\Response
     */
    public function moreArticles()
    {
        $sampleJson = '{"status":1,"data":[{"_id":"5c0de2ae2c2c734189523902","category_id":"5a66fb733730511ac1591a52","image":"\/files\/shares\/articles\/December 2018\/KDU PG\/alumni first.jpg","name":"KDU Penang Alumni Who Strike it Out on Their Own","url":"kdu-penang-alumni-who-strike-it-out-on-their-own","author":"Uni Enrol","description":"Get to know these entrepreneurs who took bold steps to carve their own destiny.","tags":["kdu penang","alumni","entrepreneur","higher education"],"created_at":"2018-12-10 11:51:10","category_name":"Post-SPM","publish_date_human":"4 weeks ago"},{"_id":"5c0de92d2c2c7348c85fbe12","category_id":"5a66fb733730511ac1591a52","image":"\/files\/shares\/articles\/December 2018\/KDU PG\/skills first.jpg","name":"5 Critical Skills You Can Sharpen from KDU Penang\u2019s Courses","url":"5-critical-skills-you-can-sharpen-from-kdu-penang\u2019s-courses","author":"Uni Enrol","description":"University is the best platform you get once in your life to experiment and explore your potential.","tags":["kdu penang","higher education","soft skills"],"created_at":"2018-12-10 12:18:53","category_name":"Post-SPM","publish_date_human":"4 weeks ago"},{"_id":"5c0e0f562c2c7374c85cc392","category_id":"5a66fb733730511ac1591a52","image":"\/files\/shares\/articles\/December 2018\/KDU PG\/biz 1.jpg","name":"6 Ways KDU Penang\u2019s School of Accounting & Business is Evolving","url":"6-ways-kdu-penang\u2019s-school-of-accounting-business-is-evolving","author":"Uni Enrol","description":"Learn why business education at KDU Penang is benchmarked to industry expectations.","tags":["kdu penang","higher education","business","accounting","post-spm"],"created_at":"2018-12-10 15:01:42","category_name":"Post-SPM","publish_date_human":"4 weeks ago"},{"_id":"5c0e19f72c2c73068d61b392","category_id":"5a66fb733730511ac1591a52","image":"\/files\/shares\/articles\/December 2018\/KDU PG\/built main.jpg","name":"7 Things to Know About KDU Penang\u2019s Interior Architecture & Interior Design Courses","url":"7-things-to-know-about-kdu-penang\u2019s-interior-architecture-interior-design-courses","author":"Uni Enrol","description":"Rapid economic growth in developing nations in Asia presents big opportunities for architects and designers.","tags":["kdu penang","higher education","architecture","interior design","post-spm"],"created_at":"2018-12-10 15:47:03","category_name":"Post-SPM","publish_date_human":"4 weeks ago"},{"_id":"5c0e1f642c2c730cea1969d2","category_id":"5a66fb733730511ac1591a52","image":"\/files\/shares\/articles\/December 2018\/KDU PG\/mass main.jpg","name":"How Mass Communication at KDU Penang Gets Your Career Going","url":"how-mass-communication-at-kdu-penang-gets-your-career-going","author":"Uni Enrol","description":"The world is moving fast and educators are moving towards more fun and engaging learning. \u00a0","tags":["kdu penang","higher education","mass communication","post-spm"],"created_at":"2018-12-10 16:10:12","category_name":"Post-SPM","publish_date_human":"4 weeks ago"},{"_id":"5c0e24672c2c7318ef079e12","category_id":"5a66fb733730511ac1591a52","image":"\/files\/shares\/articles\/December 2018\/KDU PG\/eng first.jpg","name":"What You Get Studying Engineering in the Heart of Malaysia\u2019s Technology Hub","url":"what-you-get-studying-engineering-in-the-heart-of-malaysia\u2019s-technology-hub","author":"Uni Enrol","description":"The advantage studying engineering in Penang is access to innovative technology companies","tags":["kdu penang","higher education","engineering","post-spm"],"created_at":"2018-12-10 16:31:35","category_name":"Post-SPM","publish_date_human":"4 weeks ago"}]}';

        return $sampleJson;
    }
}
