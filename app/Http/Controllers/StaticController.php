<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class StaticController extends Controller
{
    /**
     * Show About Us page.
     *
     * @return \Illuminate\Http\Response
     */
    public function aboutUs()
    {
        return view('about-us');
    }

    /**
     * Show Login page.
     *
     * @return \Illuminate\Http\Response
     */
    public function scholarSearch()
    {
        return view('scholar-search');
    }

    /**
     * Show Not Found page.
     *
     * @return \Illuminate\Http\Response
     */
    public function notFound()
    {
        return view('not-found');
    }

    /**
     * Show Server Error page.
     *
     * @return \Illuminate\Http\Response
     */
    public function serverError()
    {
        return view('server-error');
    }

    /**
     * Show Career page.
     *
     * @return \Illuminate\Http\Response
     */
    public function career()
    {
        return view('career');
    }

    /**
     * Show Article page.
     *
     * @return \Illuminate\Http\Response
     */
    public function article()
    {
        return view('article');
    }

    /**
     * Show Course Landing page.
     *
     * @return \Illuminate\Http\Response
     */
    public function courseLanding()
    {
        return view('course-landing');
    }

    /**
     * Show Course page.
     *
     * @return \Illuminate\Http\Response
     */
    public function course()
    {
        return view('course');
    }

    /**
     * Show Guide Landing page.
     *
     * @return \Illuminate\Http\Response
     */
    public function guideLanding()
    {
        return view('guide-landing');
    }

    /**
     * Show Guide page.
     *
     * @return \Illuminate\Http\Response
     */
    public function guide()
    {
        return view('guide');
    }

    /**
     * Show Contact Us page.
     *
     * @return \Illuminate\Http\Response
     */
    public function contactUs()
    {
        return view('contact-us');
    }

    /**
     * Show Compare page.
     *
     * @return \Illuminate\Http\Response
     */
    public function compare()
    {
        return view('compare');
    }

    /**
     * Show Survey page.
     *
     * @return \Illuminate\Http\Response
     */
    public function survey()
    {
        return view('survey');
    }

    /**
     * Show Privacy page.
     *
     * @return \Illuminate\Http\Response
     */
    public function privacy()
    {
        return view('privacy');
    }

    /**
     * Show TestPrep Landing page.
     *
     * @return \Illuminate\Http\Response
     */
    public function testprepLanding()
    {
        return view('testprep-landing');
    }

    /**
     * Show TestPrep page.
     *
     * @return \Illuminate\Http\Response
     */
    public function testprep()
    {
        return view('testprep');
    }

    /**
     * Show Calendar page.
     *
     * @return \Illuminate\Http\Response
     */
    public function calendarTools()
    {
        return view('calendar-tools');
    }

    /**
     * Show Timetable page.
     *
     * @return \Illuminate\Http\Response
     */
    public function timetable()
    {
        return view('timetable');
    }
}
