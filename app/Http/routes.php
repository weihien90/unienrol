<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function() {
    return view('welcome');
});

Route::get('about-us', ['uses' => 'StaticController@aboutUs', 'as' => 'aboutUs']);
Route::get('scholar-search', ['uses' => 'StaticController@scholarSearch', 'as' => 'scholarSearch']);
Route::get('not-found', ['uses' => 'StaticController@notFound', 'as' => 'notFound']);
Route::get('server-error', ['uses' => 'StaticController@serverError', 'as' => 'serverError']);
Route::get('career', ['uses' => 'StaticController@career', 'as' => 'career']);
Route::get('article', ['uses' => 'StaticController@article', 'as' => 'article']);
Route::get('course-landing', ['uses' => 'StaticController@courseLanding', 'as' => 'courseLanding']);
Route::get('course', ['uses' => 'StaticController@course', 'as' => 'course']);
Route::get('guide-landing', ['uses' => 'StaticController@guideLanding', 'as' => 'guideLanding']);
Route::get('guide', ['uses' => 'StaticController@guide', 'as' => 'guide']);
Route::get('contact-us', ['uses' => 'StaticController@contactUs', 'as' => 'contactUs']);
Route::get('compare', ['uses' => 'StaticController@compare', 'as' => 'compare']);
Route::get('survey', ['uses' => 'StaticController@survey', 'as' => 'survey']);
Route::get('privacy', ['uses' => 'StaticController@privacy', 'as' => 'privacy']);
Route::get('testprep-landing', ['uses' => 'StaticController@testprepLanding', 'as' => 'testprep-landing']);
Route::get('testprep', ['uses' => 'StaticController@testprep', 'as' => 'testprep']);
Route::get('calendar-tools', ['uses' => 'StaticController@calendarTools', 'as' => 'calendarTools']);
Route::get('timetable', ['uses' => 'StaticController@timetable', 'as' => 'timetable']);

Route::get('more-articles', ['uses' => 'ApiController@moreArticles', 'as' => 'moreArticlesApi']);