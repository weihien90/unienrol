$(document).ready(function () {
  $('#show-more-badge').click( function() {
    $(this).hide();
    $('.badge-light').removeClass('d-none');
  });

  // Re-initialize Sharethis buttons on modal shown
  $('#shareModal').on('shown.bs.modal', function (e) {
    window.__sharethis__.initialize();
  });
});