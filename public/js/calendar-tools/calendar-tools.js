$(document).ready(function () {
  $('#go-top').click(function() {
    $('html, body').animate({scrollTop : 0},800);
  });

  $('.schedule-box').click(function() {
    var date = $(this).find('.exam-date').data('date');
    if ( $(".date-" + date).length ) {
      $([document.documentElement, document.body]).animate({
        scrollTop: $(".date-" + date).offset().top - $(".date-" + date).height()
      }, 500);
    }
  });

  var examFilters = new Vue({
    el: '.filters',
    data: {
        type: "",
    },
    methods: {
        changeType: function (type) {
          this.type = type;
        },
    }
  })
});