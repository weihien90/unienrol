$(document).ready(function () {
    // Initialize the Compare Carousel
    var compareCarousel = $("#compare-carousel").owlCarousel({
        loop: false,
        nav: true,
        dots: false,
        navText: ["<i class='fa fas fa-angle-left text-primary'></i>","<i class='fa fas fa-angle-right text-primary'></i>"],
        navClass: ['list-result-box-owl-prev', 'list-result-box-owl-next'],
        autoWidth: false,
        responsive:{
            0:{
                items:2
            },
            768:{
                items:2
            },
            992:{
                items:2,
                autoWidth: true,
            },
            1440:{
                items:3,
                autoWidth: true,
            },
            1800:{
                items:4,
                autoWidth: true,
            }
        }
    });

    // Initialize the sticky-top Carousel
    var compareCarouselFixed = $("#compare-carousel-fixed").owlCarousel({
        loop: false,
        nav: false,
        dots: false,
        autoWidth: false,
        mouseDrag: false,
        touchDrag: false,
        responsive:{
            0:{
                items:2
            },
            768:{
                items:2
            },
            992:{
                items:2,
                autoWidth: true,
            },
            1440:{
                items:3,
                autoWidth: true,
            },
            1800:{
                items:4,
                autoWidth: true,
            }
        }
    });

    // Toggle Collapsible Chevron icon
    $('.section-header').click( function() {
        var $chevron = $(this).find('i');
        if ($chevron.hasClass('fa-angle-up')) {
            $chevron.attr('class', 'fa fa-angle-down font-bold ml-2');
        } else {
            $chevron.attr('class', 'fa fa-angle-up font-bold ml-2');
        }
    });

    // Delete item on Carousel
    $('#deleteCourseModal').on('show.bs.modal', function(e) {
        var $closeButton = $(e.relatedTarget);
        var index = $closeButton.parent().parent().parent().index();
        var $modal = $(this);

        $(this).find('.confirm-remove').one('click', function() {
            $("#compare-carousel").trigger('remove.owl.carousel', [index]).trigger('refresh.owl.carousel');
            $("#compare-carousel-fixed").trigger('remove.owl.carousel', [index]);

            $modal.modal('hide');
        });
    });

    // Recalculate values on other sections when PTPTN % changed
    $('input:radio[name="ptptn"]').change( function() {
        var ptptnPercentage = $(this).attr('id').slice(0, -3);
        var total = 100;
        var calculatedText = (total * ptptnPercentage) + ' (' + ptptnPercentage + '%)';

        $('.ptptn-loan-amount').text(calculatedText);
    });

    // Show the top sticky bar when scroll past certain point
    var topofDiv = $(".top-container:last").offset().top;
    var height = $(".top-container:last").outerHeight();
    $(window).scroll(function(){
        if($(window).scrollTop() > (topofDiv + height)){
            $("#top-sticky-bar").removeClass('d-none');
        }
        else{
            $("#top-sticky-bar").addClass('d-none');
        }
    });

    // Sync the Compare Carousel with the fixed-top Carousel
    compareCarousel.on('change.owl.carousel', function(event) {
        if (event.namespace && event.property.name === 'position') {
            var target = event.relatedTarget.relative(event.property.value, true);
            compareCarouselFixed.owlCarousel('to', target, 300, true);
        }
    })
});