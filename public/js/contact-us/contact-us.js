$(document).ready(function () {
  $('#sp-file').change(function(event) {
    if (event.target.files[0]) {
      var fileName = event.target.files[0].name;
      $("#selected-file").html(fileName);
      $("#selected-file-container").removeClass('invisible');
    } else {
      $("#selected-file-container").addClass('invisible');
    }
  });

  $('#selected-file-container').on('click', '#remove-file', function() {
    $('#sp-file').val('');
    $("#selected-file-container").addClass('invisible');
  });
});