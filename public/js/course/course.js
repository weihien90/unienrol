$(document).ready(function () {
    $("#universities-carousel").owlCarousel({
        loop: true,
        nav: false,
        dots: true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:2
            },
            992:{
                loop: false,
                items:4,
                dots: false
            }
        }
    });

    $("#opportunities-carousel").owlCarousel({
        loop: true,
        nav: false,
        dots: true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:2
            },
            992:{
                loop: false,
                items:4,
                dots: false
            }
        }
    });
});