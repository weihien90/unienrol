$(document).ready(function () {
    if( $('.grid').length ) {
        $('.grid').masonry({
            itemSelector: '.grid-item',
            columnWidth: '.grid-sizer',
            percentPosition: true,
            gutter: 5
        });
    }

    var moreArticles = new Vue({
        el: '.latest-articles',
        data: {
            articles: [],
            loading: false
        },
        methods: {
            loadMore: function () {
                this.loading = true;
                var self = this;
                $.getJSON('/more-articles', function(res) {
                    console.log(res.data);
                    self.articles = self.articles.concat(res.data);
                    self.loading = false;
                });
            },
        }
    })
});

Vue.config.devtools = true;