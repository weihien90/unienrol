$(document).ready(function () {
    // Toggle dropdown Chevron icon
    $('.dropdown').on('show.bs.dropdown', function () {
        $(this).find('i').attr('class', 'fa fa-chevron-up');
    }).on('hide.bs.dropdown', function () {
        $(this).find('i').attr('class', 'fa fa-chevron-down');
    });

    // Toggle collapse Chevron icon
    $('.collapsible-menu').on('show.bs.collapse', function () {
        console.log($(this));
        $(this).find('i').attr('class', 'fa fa-chevron-up');
    }).on('hide.bs.collapse', function () {
        $(this).find('i').attr('class', 'fa fa-chevron-down');
    });

    $('.notification').click( function() {
        $(this).removeClass('notification');
    });
});