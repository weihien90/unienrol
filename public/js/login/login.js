var hasUser = 0;
var signup = 0;
//Login, Signup Modal
function loginModal() {
    loginlink.modal_type = 'login';
    loginlink.openModal();
}
$(document).ready(function () {
    window.loginlink = new Vue({
        el : '#loginlink',
        data : {
            showModal : false,
            modal_type : 'login'
        },
        methods: {
            openModal: function () {
                this.showModal = !this.showModal;
                document.body.style.overflow = this.showModal ? 'hidden' : 'visible';
            }
        }
    });
    
    var login_error = $.parseJSON(false)
    if (login_error) {
        loginlink.modal_type = 'login';
        loginlink.openModal();
    } else if (signup) {
        loginlink.modal_type = 'signup';
        loginlink.openModal();
    }
});

var login_session = 0;
var loginmodal_token = "miDP6RM1b8z32plAJAZox0oq8AQHtBeCvKhlrMOh";
var loginmodalsession_url = "https://unienrol.com/session/login";

//Register VueJS Component
Vue.component('loginModal', {
    template: '#login-modal-template',
    props: ["modal_type"],
    data: function () {
    return {
      formInputs: {},
      formErrors: {},
      // modal_type: this.props.modal_type,
    }
  },
  methods: {
      submitLogin: function(e) {
          $('.login-spinner').show();
          e.preventDefault();
          var form = e.target || e.srcElement;
          var action = form.action;
          var csrfToken = form.querySelector('input[name="_token"]').value;
          this.$http.post(action, this.formInputs, {
              headers: {
                  'X-CSRF-TOKEN': csrfToken
              }
          })
          .then(function() {
              this.modalsession('open')
              form.submit();
              delete this.formInputs["password"];
              // userActivities('login',null,'email',null,this.formInputs);
          })
          
          .catch(function (data, status, request) {
              $('.login-spinner').hide();
              this.formErrors = data.data;
          });
      },
      submitSignup: function(e) {
          $('.signup-spinner').show();
          e.preventDefault();
          var form = e.srcElement;
          var action = form.action;
          var csrfToken = form.querySelector('input[name="_token"]').value;
          this.$http.post(action, this.formInputs, {
              headers: {
                  'X-CSRF-TOKEN': csrfToken
              }
          })
          .then(function() {
              this.modalsession('open')
              form.submit();
              delete this.formInputs["password"];
              delete this.formInputs["password_confirmation"];
              // userActivities('signup',null,'email',null,this.formInputs);
          })
          
          .catch(function (data, status, request) {
             $('.signup-spinner').hide();
              this.formErrors = data.data;
              // console.log(this.formErrors['email']);
          });
      },
      close: function () {        
        //this.modalsession('close')
        loginlink.openModal();
        this.$emit('close');
      },
      modalsession: function(status) {
        var token = loginmodal_token;
        var url = loginmodalsession_url;
        this.$http.post(url, { status: status, modal_type: this.modal_type }, {
          headers: {
                  'X-CSRF-TOKEN': token
              }
            })
      },
      openLogin: function () {        
        displayLoginModal();
        removeError();
      },
      openSignup: function () {  
        // userActivities('click','login','create account');
        displaySignUpModal();
        removeError();
      },
  },
  mounted(){
    if(this.modal_type == 'signup'){
      displaySignUpModal();
    }
  }
})

  $(document).ready(function(){
    if(login_session == 1){
      displayLoginModal();
    }

    if($('#errors').data('value') == "1"){
      $('#errors').show().delay(5000).fadeOut('fast'); 
    }

  });

  // $(document).on('focus', '.login-btn', function() {
  //   displayLoginModal();
  //   removeError();
  // });
  // $(document).on('focus', '.signup-btn', function() {
  //   displaySignUpModal();
  //   removeError();
  // });
  function removeError(){
    $(".error").each(function() {
      $(this).remove();
    });
  }
  function displayLoginModal(){
    $("#login-form").show();
    $("#signup-form").hide();
    $(".login-footer").hide();
    $(".signup-footer").show();
  }

  function displaySignUpModal(){
    $("#login-form").hide();
    $("#signup-form").show();
    $(".login-footer").show();
    $(".signup-footer").hide();
  }