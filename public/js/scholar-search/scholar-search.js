$(document).ready(function () {
    $("#faq-carousel").owlCarousel({
        loop: true,
        nav:true,
        dots: false,
        navText: ["<i class='fa fas fa-angle-left text-primary'></i>","<i class='fa fas fa-angle-right text-primary'></i>"],
        navClass: ['list-result-box-owl-prev', 'list-result-box-owl-next'],
        responsive:{
            0:{
                items:1,
                margin:10,
                dots: true,
                nav: false
            },
            576:{
                items: 2,
                margin:10,
                nav: false,
                dots: true,
            },
            768:{
                items:3,
                margin:50
            },
            992:{
                items:3,
                margin: 100
            }
        }
    });

    $("#header-carousel").owlCarousel({
        loop: true,
        nav:true,
        dots: false,
        navText: ["<i class='fa fas fa-angle-left text-primary'></i>","<i class='fa fas fa-angle-right text-primary'></i>"],
        navClass: ['list-result-box-owl-prev ml-5', 'list-result-box-owl-next mr-5'],
        items:1,
        responsive:{
            0:{
                nav: false,
                dots: true,
            },
            576:{
                dots: false,
                nav: true 
            }
        }
    });
});