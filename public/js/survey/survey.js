$(document).ready(function () {
    window.surveylink = new Vue({
        el : '#surveylink',
        data : {
            showSurveyModal : false
        },
        methods: {
            toggleModal: function () {
                this.showSurveyModal = !this.showSurveyModal;
            }
        }
    });
});

//Register VueJS Component
Vue.component('surveyModal', {
    template: '#survey-modal-template',
    props: [],
    data: function () {
        return {
            step: 0,
            response: {
                type: '',
                country: '',
                state: '',
                city: '',
                secondaryQualification: '',
                school: '',
                hasResult: 'yes',
                results: {
                    'A+': '',
                    'A': '',
                    'A-': '',
                    'B': '',
                    'C': '',
                    'D': '',
                    'E': '',
                },
                otherQualification: '',
                highestQualification: '',
                phoneCountry: '+60',
                phone: ''
            }
        }
    },
    computed: {
        nextText: function() {
            return (this.step === 6 ? "Finish Survey" : "NEXT Question") + " (" + this.step + "/6)";
        },

        validated: function() {
            var res = this.response;
            switch(this.step) {
                case 1:
                    return res.country && res.state && res.city;
                case 2:
                    return res.school;
                case 3:
                    return res.secondaryQualification;
                case 4:
                    return (res.secondaryQualification === 'others' && res.otherQualification) ||
                        (res.secondaryQualification !== 'others' && Object.keys(res.results).filter( function(grade) {return res.results[grade]}) ).length ||
                        (res.hasResult === 'no');
                case 5:
                    return res.highestQualification;
                case 6:
                    return res.phone;
                default:
                    return true;
            }
        }
    },
    methods: {
        close: function () {
            surveylink.toggleModal();
            this.$emit('close');
        },

        nextStep: function() {
            this.step += 1;

            if (this.step === 7) {
                this.close();
            }
        },

        selectType: function(type) {
            this.response.type = type;
            this.nextStep();
        }
    }
})