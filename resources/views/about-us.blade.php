@extends('layouts.app')

@section('custom_css')
    <link href="{{ url('css/about-us/about-us.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<section class="about-us introduction">
    <div class="container">
        <div class="row">
            <div class="col-12 mt-3 mt-md-5">
                <span class="section-title font-light">INTRODUCING UNI ENROL</span>
                <div class="float-md-right text-center">
                    <button class="btn btn-primary font-semibold px-5 py-2 mt-3 mt-md-0">Sign Up Today</button>
                </div>
            </div>

            <div class="section-description border-top-primary border-bottom-primary mt-3 p-3">
                <p>Prologues - Education is a powerful enabler that allows you to be who you want to be.</p>
                <p>People say university years are the best years of our life.</p>
                <p>For most students, the first year of university can be both a 
                daunting and exciting experience. It is a series of firsts in decision-making to many – to make THE decision on WHICH DEGREE 
                to pursue; to decide WHICH UNIVERSITY to study in or whether to just follow the crowd; to choose either a subject that you 
                have been excelling at or are truly passionate about. For others, it is a change in environment where you can wipe the slate 
                clean and begin again in building a social circle of like-minded peers, or start anew in self-discovery and reinvention.</p>
                <p>At Uni Enrol, we believe that students should be empowered with the necessary information to make a well-informed decision.</p>
                <p>We also understand that the plethora of information out there can overconsume the attention of many students and cost them to 
                miss crucial opportunities available. Our role is to provide the right tools and resources to efficiently allocate and condense 
                the universities, courses, education pathways and financial assistance that are available to each student based on their eligibility.</p>
                <p>That way, students can now have full visibility to choose a course and university that they aspire to.</p>
            </div>
        </div>
    </div>
</section>

<section class="about-us feature">
    <div class="container">
        <div class="row my-5">
            <div class="col-md-4 mb-3 text-center">
                <div class="feature-img d-inline-block mb-3"><img src="{{ url('img/about-us/ic_technology.svg') }}"></div>
                <div class="feature-title font-medium mb-2"><span>Technology</span></div>
                <div class="feature-desc font-medium">
                    <span>Technology will continue to be the core in improving your experience with us. We leverage on technology to make your search for the 
                        right course, up to its application, simple and easy.</span>
                </div>
            </div>

            <div class="col-md-4 mb-3 text-center">
                <div class="feature-img d-inline-block mb-3"><img src="{{ url('img/about-us/ic_support.svg') }}"></div>
                <div class="feature-title font-medium mb-2"><span>Support</span></div>
                <div class="feature-desc font-medium">
                    <span>You are not alone in this journey of discovery. Our team of experienced counsellors are always reachable to guide you along the way. 
                        Other support comes in the form of insightful content to help you better understand who you are and the necessary considerations in your 
                        decision-making.</span>
                </div>
            </div>

            <div class="col-md-4 mb-3 text-center">
                <div class="feature-img d-inline-block mb-3"><img class="img-fluid" src="{{ url('img/about-us/ic_partners.svg') }}"></div>
                <div class="feature-title font-medium mb-2"><span>Partners</span></div>
                <div class="feature-desc font-medium">
                    <span>Students come from all walks of life and background. That is why we work with awesome partners to provide various forms of support 
                        including scholarships and financial aids to enable you to attain quality education of your choice.</span>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-us cta">
    <div class="container">
        <div class="row">
            <div class="col-12 mt-3 text-center mb-5">
                <span class="section-desc d-block font-medium mb-2">Start exploring Uni Enrol</span>
                <span class="section-desc d-block font-medium mb-3">We are here to assist you in every step of the way</span>
                <button class="btn text-center btn-primary font-semibold px-5 py-2">Get In Touch with us Today</button>
            </div>
        </div>
    </div>
</section>
@endsection