@extends('layouts.app')

@section('custom_css')
    <link href="{{ url('css/article/article.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('custom_js')
    <script src="{{ url('js/article/article.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c445b6b7224d400111824d3&product=inline-share-buttons' async='async'></script>
@endsection

@section('content')
<section class="article main mt-4">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-between flex-wrap">
                <h1>Education Pathway to Engineering – Turning Ideas Into Innovation</h1>
                <span class="section-small-title font-medium align-self-end">By Jie Min - August 23, 2018</span>
            </div>
            <hr />
        </div>

        <div class="mb-3 pb-2">
            <div class="row">
                <div class="col-12 mb-3">
                    <span class="badge badge-danger">Degree Pathways</span>
                    <span class="pl-md-2 mt-2 d-block d-md-inline-block font-medium">Engineering knowledge enables you to create mind blowing things</span>
                </div>

                <div class="col-12 mb-3">
                    <a href="#" class="badge badge-light">Actuarial Science</a>
                    <a href="#" class="badge badge-light">Banking & finance</a>
                    <a href="#" class="badge badge-light">Business Adminstration</a>
                    <a href="#" class="badge badge-light">Business & Management</a>
                    <a href="#" class="badge badge-light">Economics</a>
                    <a href="#" class="badge badge-light d-md-none" id="show-more-badge">Show all+</a>
                    <a href="#" class="badge badge-light d-none d-md-inline-block">Other 1</a>
                    <a href="#" class="badge badge-light d-none d-md-inline-block">Other 2</a>
                </div>

                <div class="col-12 mb-4 mb-md-5">
                    <img class="w-100" src="https://via.placeholder.com/1229x492">                    
                </div>

                <div class="col-12">
                    <div class="article-content">
                        <h3>What is Architecture? (H3)</h3>
                        
                        <p>
                            Where we are today having the comfort and convenience such as mobile phones, aeroplanes, elevators, and washing machines is all 
                            thanks to <b>innovative engineers</b>. Becoming a skilled engineer enables you to invent and create amazing things. Think Tesla, NASA, 
                            Apple, Samsung, Dyson, General Electric and many more. Students can consider PTPTN loan to cover any additional amount which 
                            may not be covered by scholarship. PTPTN only charges 1% interest rate and you only start repaying once you graduate. PTPTN 
                            loan is a student education aid disbursed by <b>Perbadanan Tabung Pendidikan Tinggi Nasional</b>, which is a government institution 
                            established to help students to pursue higher education.
                        </p>

                        <div class="alert alert-shoutout" role="alert">
                            <h3>What Is Uni Enrol? (H3 Italic)</h3>
                            <p>
                                Uni Enrol is an online platform that matches you with courses, scholarships and bursaries. Drop us a message and <a href="#">let us help you.</a>
                            </p>
                        </div>

                        <p>
                            Where we are today having the comfort and convenience such as mobile phones, aeroplanes, elevators, and washing machines is all 
                            thanks to innovative engineers. Becoming a skilled engineer enables you to invent and create amazing things. Think Tesla, NASA, 
                            Apple, Samsung, Dyson, General Electric and many more. Students can consider PTPTN loan to cover any additional amount which <b>may 
                            not be covered by scholarship</b>. PTPTN only charges 1% interest rate and you only start repaying once you graduate.
                        </p> 
                        
                        <p>
                            PTPTN loan is a student education aid disbursed by Perbadanan Tabung Pendidikan Tinggi Nasional, which is a government 
                            institution established to help students to pursue higher education.
                        </p>

                        <div class="text-center">
                            <figure class="figure">
                                <img src="https://via.placeholder.com/500x250" class="figure-img">
                                <figcaption class="figure-caption">A caption for the above image.</figcaption>
                            </figure>
                        </div>

                        <p>
                            Where we are today having the comfort and convenience such as mobile phones, aeroplanes, elevators, and washing machines is all thanks to 
                            innovative engineers. Becoming a skilled engineer enables you to invent and create amazing things. Think Tesla, NASA, Apple, Samsung, Dyson, 
                            General Electric and many more. Students can consider PTPTN loan to cover any additional amount which <b>may not be covered by scholarship.</b> 
                            PTPTN only charges 1% interest rate and you only start repaying once you graduate.
                        </p>
                            
                        <p>
                            PTPTN loan is a student education aid disbursed by Perbadanan Tabung Pendidikan Tinggi Nasional, which is a government institution 
                            established to help students to pursue higher education.
                        </p>

                        <h2>Scholarships/Bursaries with Instant Approval Using Forecast/Trial or Actual Results (H2)</h2>

                        <ol>
                            <li>
                                <h3>Pre-University & Foundation Courses</h3>
                                <b>Scholarship Name</b> : Taylor’s Excellence Award – Pre-U & Foundation<br>
                                <b>Course Coverage</b> : Cambridge A-Levels, SACE International (SACEi), All Foundation<br>
                                <b>Total Tuition Fees</b> : RM33,000 (A-Level), RM24,000 (SACEi), RM24,500 (Foundation) This award is purely based on student’s academic results.
                            </li>

                            <li>
                                <h3>Pre-University & Foundation Courses</h3>
                                <b>Scholarship Name</b> : Taylor’s Excellence Award – Pre-U & Foundation<br>
                                <b>Course Coverage</b> : Cambridge A-Levels, SACE International (SACEi), All Foundation<br>
                                <b>Total Tuition Fees</b> : RM33,000 (A-Level), RM24,000 (SACEi), RM24,500 (Foundation) This award is purely based on student’s academic results.
                            </li>
                        </ol>

                        <table class="table">
                            <caption>Uni Enrol is an online platform that matches you with courses, scholarships and bursaries. Drop us a message and <a href="#">let us help you.</a></caption>
                            <thead>
                                <tr>
                                    <th scope="col">Level Of Study</th>
                                    <th scope="col" colspan="3">Random Title Here</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <th scope="row">Diploma</th>
                                    <td>6,800</td>
                                    <td>5,100</td>
                                    <td>3,400</td>
                                </tr>
                                <tr>
                                    <th scope="row">Diploma</th>
                                    <td>6,800</td>
                                    <td>5,100</td>
                                    <td>3,400</td>
                                </tr>
                                <tr>
                                    <th scope="row">Diploma</th>
                                    <td>6,800</td>
                                    <td>5,100</td>
                                    <td>3,400</td>
                                </tr>
                                <tr>
                                    <th scope="row">Diploma</th>
                                    <td>6,800</td>
                                    <td>5,100</td>
                                    <td>3,400</td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-vertical">
                            <caption>Uni Enrol is an online platform that matches you with courses, scholarships and bursaries. Drop us a message and <a href="#">let us help you.</a></caption>
                            <thead>
                                <tr>
                                    <th scope="col">Level Of Study</th>
                                    <th scope="col" colspan="3">Random Title Here</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <th scope="row">Diploma</th>
                                    <td>6,800</td>
                                    <td>5,100</td>
                                    <td>3,400</td>
                                </tr>
                                <tr>
                                    <th scope="row">Diploma</th>
                                    <td>6,800</td>
                                    <td>5,100</td>
                                    <td>3,400</td>
                                </tr>
                                <tr>
                                    <th scope="row">Diploma</th>
                                    <td>6,800</td>
                                    <td>5,100</td>
                                    <td>3,400</td>
                                </tr>
                                <tr>
                                    <th scope="row">Diploma</th>
                                    <td>6,800</td>
                                    <td>5,100</td>
                                    <td>3,400</td>
                                </tr>
                            </tbody>
                        </table>

                        <p>
                            Where we are today having the comfort and convenience such as mobile phones, aeroplanes, elevators, and washing machines is all thanks to 
                            innovative engineers. Becoming a skilled engineer enables you to invent and create amazing things. Think Tesla, NASA, Apple, Samsung, Dyson, 
                            General Electric and many more. Students can consider PTPTN loan to cover any additional amount which <b>may not be covered by scholarship.</b> 
                            PTPTN only charges 1% interest rate and you only start repaying once you graduate.
                        </p>

                        <p>
                            PTPTN loan is a student education aid disbursed by Perbadanan Tabung Pendidikan Tinggi Nasional, which is a government institution 
                            established to help students to pursue higher education.
                        </p>

                        <div class="alert alert-custom" role="alert">
                            <b>Uni Enrol</b> is an online platform that matches you with <b>courses, scholarships and bursaries</b>. rem ipsum dolor amet edison bulb wayfarers chicharrones bespoke etsy banjo.<br/>
                            <a href="#">Click here to learn more ></a>
                        </div>

                        <div class="alert alert-success" role="alert">
                            <a href="#">Click here</a> to apply
                            <ul>
                                <li>One</li>
                                <li>Two</li>
                                <li>Three</li>
                            </ul>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="article consult mb-4 py-4">
    <div class="container full-height">
        <div class="row">
            <div class="col-12">
                <span class="section-title font-light">Have Question? Consult our Counsellors</span>
            </div>
            <div class="hr col-12 mt-2 mb-4 mx-3">
            </div>
        </div>
        <div class="form-row">
            <div class="col-12 col-md-10 mb-2">
                <div class="text-input-container">
                    <label for="name" class="font-medium">Name</label>
                    <input class="form-control py-4" name="name" id="name" placeholder="John Doe" />
                </div>
            </div>
            <div class="col-12 col-md-10 col-lg-5 mb-2">
                <div class="text-input-container">
                    <label for="phone" class="font-medium">Phone Number</label>
                    <input class="form-control py-4" name="phone" id="phone" placeholder="010 12345678" />
                </div>
            </div>
            <div class="col-12 col-md-10 col-lg-5 mb-2">
                <div class="text-input-container">
                    <label for="email" class="font-medium">Email</label>
                    <input class="form-control py-4" name="email" id="email" placeholder="johndoe@gmail.com" />
                </div>
            </div>
            <div class="col-12 col-md-10 mb-4">
                <div class="textarea-input-container">
                    <label for="message" class="font-medium">Message</label>
                    <textarea class="form-control" placeholder="Your Question"></textarea>
                </div>
            </div>
            <div class="col-12 mb-3">
                <div class="form-group px-2">
                    <label class="font-medium" id="subscribe">
                        <input type="checkbox" name="subscribe"> Subscribe to Uni Enrol Exclusive Newsletter to get the most up to date scholar deals!
                    </label>
                    <div class="d-flex flex-wrap justify-content-md-between justify-content-center align-items-center">
                        <div class="g-recaptcha" data-sitekey="6LeUbHsUAAAAADgU-cvzorA-Ug_pWkOafFOfu3uT"></div>
                        <button class="btn btn-primary font-semibold px-5 py-2 mt-3 mt-md-0" type="submit">SUBMIT YOUR MESSAGE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="article recommends my-5">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-2">
                <span class="section-title font-light">Other articles you might be interested in</span>
            </div>
            <div class="hr col-12 mt-3"></div>
            @for ($i = 0; $i < 4; $i++)                
            <div class="col-md-6 col-lg-3 my-3 my-lg-4 px-md-4">
                <div class="article-img mb-3">
                    <img class="w-100 img-fluid" src="https://via.placeholder.com/277x161">
                </div>
                <div class="article-title font-light mb-3">
                    <span>What Do You Get From a Full Scholarship at LIMKOKWING University</span>
                </div>
                <div class="article-desc">
                    <span>An amazing experience awaits you at Limkokwing University</span>
                </div>
            </div>
            @endfor
        </div>
    </div>
</section>
@endsection