@extends('layouts.app')

@section('custom_css')
    <link href="{{ url('css/calendar-tools/calendar-tools.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('custom_js')
    <script src="{{ url('js/calendar-tools/calendar-tools.js') }}"></script>
@endsection

@section('content')
<section class="calendar-tools header">
    <div class="container full-height">
        <div class="row pt-2 pb-4 py-md-5">
            <div class="col-12 mt-1">
                <span class="header-title font-semibold d-block">Uni Enrol Calendar Tools</span>
                <span class="header-desc d-block">Stay aware and ahead on your Exam Schedules</span>
                <hr />
                <span class="header-desc d-lg-inline-block d-block">Check out Uni Enrol exclusive SPM test prep online tool today</span>
                <button class="btn btn-primary font-semibold px-4 px-md-5 py-2 mt-2 float-lg-right">TEST PREP</button>
            </div>
        </div>
    </div>
</section>

<section class="calendar-tools exam-schedules mb-5">
    <div class="container">
        <div class="row">
            <div class="col-12 mt-3">
                <span class="section-title font-light">Exam Schedules</span>
                <hr />
            </div>
        </div>

        <div class="row no-gutters">
            @for ($i=0; $i<6; $i++)
            <div class="col-6 col-sm-4 col-lg-2 pr-3 mb-2">
                <div class="schedule-box d-flex flex-column justify-content-center align-items-center py-4">
                    <span class="exam-name font-semibold">SPM</span>
                    <span class="exam-year font-light">2019</span>
                </div>
            </div>
            @endfor
        </div>
    </div>
</section>

<section class="calendar-tools filters">
    <div class="container">
        <div class="row">
            <div class="col-12 mt-3">
                <span class="section-title font-light">Calendar Filters</span>
                <div class="float-right mt-2 font-medium" id="clear-filter" @click="changeType('')">
                    <span class="mr-1">x</span> Clear Filter
                </div>
                <hr />
            </div>
        </div>

        <div class="row" id="filter-buttons">
            <div class="col-12 mt-3">
                <button type="button" @click="changeType('mohe-upu')" :class="{ active: type === 'mohe-upu'}" class="btn font-medium mb-2 btn-filters btn-outline-success">MOHE UPU</button>
                <button type="button" @click="changeType('matrix')" :class="{ active: type === 'matrix'}" class="btn font-medium mb-2 btn-filters btn-outline-success">Malaysian Matriculation</button>
                <button type="button" @click="changeType('spm-spmu')" :class="{ active: type === 'spm-spmu'}" class="btn font-medium mb-2 btn-filters btn-outline-success">SPM / SPMU</button>
                <button type="button" @click="changeType('uec')" :class="{ active: type === 'uec'}" class="btn font-medium mb-2 btn-filters btn-outline-warning">UEC</button>
                <button type="button" @click="changeType('o-levels')" :class="{ active: type === 'o-levels'}" class="btn font-medium mb-2 btn-filters btn-outline-secondary">O-Levels</button>
                <button type="button" @click="changeType('a-levels')" :class="{ active: type === 'a-levels'}" class="btn font-medium mb-2 btn-filters btn-outline-info">A-Levels</button>
                <button type="button" @click="changeType('stpm')" :class="{ active: type === 'stpm'}" class="btn font-medium mb-2 btn-filters btn-outline-primary">STPM</button>
                <button type="button" @click="changeType('cimp-cpu')" :class="{ active: type === 'cimp-cpu'}" class="btn font-medium mb-2 btn-filters btn-outline-dark">CIMP / CPU</button>
                <button type="button" @click="changeType('wace')" :class="{ active: type === 'wace'}" class="btn font-medium mb-2 btn-filters btn-outline-success">WACE</button>
                <button type="button" @click="changeType('sace')" :class="{ active: type === 'sace'}" class="btn font-medium mb-2 btn-filters btn-outline-danger">SACE</button>
            </div>
        </div>

        <div class="row" id="calendar-card-container">
            <div class="col-12">
                @for ($i=0; $i<12; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'mohe-upu'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        JAN
                    </div>

                    <div class="w-100">
                        <div class="calendar-card success p-3">
                            <span class="card-date font-semibold">15 Mar 2018</span><br />
                            <span class="card-desc">Sijil Pelajaran Malaysia (SPM) 2017 Results</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor

                @for ($i=0; $i<4; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'matrix'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        JAN
                    </div>

                    <div class="w-100">
                        <div class="calendar-card success p-3">
                            <span class="card-date font-semibold">15 Mar 2018</span><br />
                            <span class="card-desc">Sijil Pelajaran Malaysia (SPM) 2017 Results</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor

                @for ($i=0; $i<4; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'spm-spmu'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        JAN
                    </div>

                    <div class="w-100">
                        <div class="calendar-card success p-3">
                            <span class="card-date font-semibold">15 Mar 2018</span><br />
                            <span class="card-desc">Sijil Pelajaran Malaysia (SPM) 2017 Results</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor

                @for ($i=0; $i<4; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'uec'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        JAN
                    </div>

                    <div class="w-100">
                        <div class="calendar-card warning p-3">
                            <span class="card-date font-semibold">15 Mar 2018</span><br />
                            <span class="card-desc">Sijil Pelajaran Malaysia (SPM) 2017 Results</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor

                @for ($i=0; $i<4; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'o-levels'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        JAN
                    </div>

                    <div class="w-100">
                        <div class="calendar-card secondary p-3">
                            <span class="card-date font-semibold">15 Mar 2018</span><br />
                            <span class="card-desc">Sijil Pelajaran Malaysia (SPM) 2017 Results</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor

                @for ($i=0; $i<4; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'a-levels'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        JAN
                    </div>

                    <div class="w-100">
                        <div class="calendar-card info p-3">
                            <span class="card-date font-semibold">15 Mar 2018</span><br />
                            <span class="card-desc">Sijil Pelajaran Malaysia (SPM) 2017 Results</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor

                @for ($i=0; $i<4; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'stpm'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        JAN
                    </div>

                    <div class="w-100">
                        <div class="calendar-card primary p-3">
                            <span class="card-date font-semibold">15 Mar 2018</span><br />
                            <span class="card-desc">Sijil Pelajaran Malaysia (SPM) 2017 Results</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor

                @for ($i=0; $i<4; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'cimp-cpu'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        JAN
                    </div>

                    <div class="w-100">
                        <div class="calendar-card dark p-3">
                            <span class="card-date font-semibold">15 Mar 2018</span><br />
                            <span class="card-desc">Sijil Pelajaran Malaysia (SPM) 2017 Results</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor

                @for ($i=0; $i<4; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'wace'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        JAN
                    </div>

                    <div class="w-100">
                        <div class="calendar-card success p-3">
                            <span class="card-date font-semibold">15 Mar 2018</span><br />
                            <span class="card-desc">Sijil Pelajaran Malaysia (SPM) 2017 Results</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor

                @for ($i=0; $i<4; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'sace'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        JAN
                    </div>

                    <div class="w-100">
                        <div class="calendar-card danger p-3">
                            <span class="card-date font-semibold">15 Mar 2018</span><br />
                            <span class="card-desc">Sijil Pelajaran Malaysia (SPM) 2017 Results</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor
            </div>
        </div>
    </div>
</section>

<button class="btn btn-primary py-1" id="go-top">
    <i class="fa fa-angle-double-up"></i>
</button>
@endsection