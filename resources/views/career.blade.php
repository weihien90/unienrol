@extends('layouts.app')

@section('custom_css')
    <link href="{{ url('css/career/career.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<section class="career mt-4">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-3">
                <span class="section-title font-light">Fashion Buyer</span>
            </div>
        </div>

        <div class="mb-3 py-3 career-nature-box">
            <div class="row">
                <div class="col-md-8 order-2 order-md-1">
                    <span class="section-sub-title font-medium d-block mt-3 mt-md-0">About</span>
                    <span class="section-description font-medium d-block">Fashion buyers decide the fashion items that a store carries each season.</span>
                    <span class="section-sub-title font-medium d-block mt-4">Nature of the job</span>
                    <span class="section-description font-medium d-block">
                        <p>If you work for major retailers like ASOS and Shopbob, chances are you will get invites to every happening fashion show in town, 
                        and sometimes out of town. It’s exciting, sitting front row with fellow socialites, fashionistas and elite magazine editors like 
                        Anna Wintour, getting first hand preview on what is to come in the fashion industry, taking photos with international designers 
                        etc, you get the idea, it’s glamourous. But that’s not the full picture.</p>
                        <p>The fashion industry is very high paced, you need to have the fashion intuition to predict what designs or features will set the 
                        trend for the upcoming season. The selection process can be done well if you have in depth understanding of your targeted customers. 
                        That aside, you always need to introduce new elements to keep things fresh. What you do is critical to the success of the company 
                        because if visitors don’t like what they see in store, chances are they won’t come back again. It gives great job satisfaction when 
                        your collection is well received by public.</p>
                        <p>You will be working very closely with your clothing suppliers and designers, so excellent interpersonal skills are a must. You may 
                        also need to travel a lot to trade shows for buying trips. Be numerate as you need to balance the budget. Outside the standard working 
                        hours, you also need to attend fashion shows or other related events in the evenings or weekends.</p>
                    </span>
                </div>

                <div class="col-md-4 order-1 order-md-2 float-md-right text-center">
                    <img class="img-fluid" src="https://via.placeholder.com/349x428">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 mb-5">
                <span class="section-sub-title font-medium d-block mb-2">Job Description</span>
                <span class="section-description font-medium d-block">
                    - Visit designers and manufacturers’ sites to decide on the items to bring in for the upcoming season<br>
                    - Negotiate for the best price and make sure to keep the buying expenses within the allocated budget<br>
                    - Monitor sales performance. Identify and restock bestselling items following market demand<br><br>

                    <span class="font-bold">Career Progress: Assistant buyer > Associate buyer > Buyer > Senior buyer > Buyer Manager</span>
                </span>
            </div>
        </div>

        <div class="row">
            <div class="col-12 mb-5">
                <span class="section-sub-title font-medium d-block mb-2">Related Majors</span>
                <button class="btn btn-outline-secondary btn-tag px-3 mr-2 py-1 mt-2 font-medium">Fashion Design</button>
                <button class="btn btn-outline-secondary btn-tag px-3 mr-2 py-1 mt-2 font-medium">Arts</button>
                <button class="btn btn-outline-secondary btn-tag px-3 mr-2 py-1 mt-2 font-medium">Marketing</button>
                <div class="text-md-right text-center mt-4">
                    <button class="btn btn-primary px-5 py-2 font-semibold">BACK</button>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection