@extends('layouts.app')

@section('custom_css')
    <link href="{{ url('css/compare/compare.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('custom_js')
    <script src="{{ url('js/compare/compare.js') }}"></script>
@endsection

@section('content')
<section class="compare main d-lg-none position-fixed w-100" id="top-fixed-bar">
    <div class="row mt-2 mx-2">
        <div class="col-12 d-flex align-items-end">
            <span class="section-title flex-grow-1">Compare</span>
            <div class="d-flex flex-nowrap">
                <a href="https://unienrol.com/facebook/redirect" class="mr-1 btn btn-lg btn-facebook share-btn d-flex justify-content-center">
                    <i aria-hidden="true" class="fa fa-facebook align-self-center"></i>
                </a>
                <a href="https://unienrol.com/facebook/redirect" class="mr-1 btn btn-lg btn-messenger share-btn d-flex justify-content-center">
                    <i aria-hidden="true" class="fab fa-facebook-messenger align-self-center"></i>
                </a>
                <a href="https://unienrol.com/facebook/redirect" class="btn btn-lg btn-url share-btn d-flex justify-content-center">
                    <i aria-hidden="true" class="fas fa-link align-self-center"></i>
                </a>
            </div>
        </div>
        <hr class="w-100 mt-1 mb-0" />
    </div>
</section>

<section class="compare main d-none" id="top-sticky-bar">
    <div class="row flex-nowrap">
        <div class="sidebar flex-shrink-0 px-5 pt-4 d-none d-lg-block">
            <h2 class="section-title font-light">Compare</h2>
            <hr />
            <h3 class="section-small-title font-semibold mb-3">Share your compare Results</h3>
            <div class="row mt-4 pl-4 justify-content-start">
                <a href="https://unienrol.com/facebook/redirect" class="mr-3 btn btn-lg btn-facebook share-btn d-flex justify-content-center">
                    <i aria-hidden="true" class="fa fa-facebook align-self-center"></i>
                </a>
                <a href="https://unienrol.com/facebook/redirect" class="mr-3 btn btn-lg btn-messenger share-btn d-flex justify-content-center">
                    <i aria-hidden="true" class="fab fa-facebook-messenger align-self-center"></i>
                </a>
                <a href="https://unienrol.com/facebook/redirect" class="btn btn-lg btn-url share-btn d-flex justify-content-center">
                    <i aria-hidden="true" class="fas fa-link align-self-center"></i>
                </a>
            </div>
        </div>

        <div style="overflow: hidden">
            <div class="owl-carousel px-lg-5" id="compare-carousel-fixed">
                @for ($i = 0; $i < 4; $i++)
                <div class="item px-1 px-sm-4 py-4 text-center">
                    <div class="bg-overlay d-flex flex-column justify-content-around py-2">
                        <span class="top-info">Information Technology ({{ $i+1 }})</span>
                        <span class="top-info">Degree</span>
                        <span class="top-info-lg font-bold">MYR 76,600</span>
                        <a href="/" class="mt-2 py-1 py-sm-3 px-4 align-self-center btn btn-outline-secondary font-semibold">APPLY</a>
                    </div>
                    <div class="item-img my-4">
                        <img class="w-100 img-fluid" src="https://via.placeholder.com/263x100">
                    </div>
                </div>
                @endfor
            </div>
        </div>
    </div>
</section>

<section class="compare main" id="main">
    <div class="row flex-nowrap">
        <div class="sidebar flex-shrink-0 px-lg-5 pt-4 pb-5">
            <div class="top-container">
                <h2 class="section-title font-light">Compare</h2>
                <hr />
                <h3 class="section-small-title font-semibold mb-3">Share your compare Results</h3>
                <div class="row align-items-center mt-4 pl-2">
                    <div class="col-3">
                        <a href="https://unienrol.com/facebook/redirect" class="btn btn-lg btn-facebook share-btn d-flex justify-content-center">
                            <i aria-hidden="true" class="fa fa-facebook align-self-center"></i>
                        </a>
                    </div>
                    <span class="share-label">Facebook</span>
                </div>
                <div class="row align-items-center mt-2 pl-2">
                    <div class="col-3">
                        <a href="https://unienrol.com/facebook/redirect" class="btn btn-lg btn-messenger share-btn d-flex justify-content-center">
                            <i aria-hidden="true" class="fab fa-facebook-messenger align-self-center"></i>
                        </a>
                    </div>
                    <span class="share-label">Messenger</span>
                </div>
                <div class="row align-items-center mt-2 pl-2">
                    <div class="col-3">
                        <a href="https://unienrol.com/facebook/redirect" class="btn btn-lg btn-url share-btn d-flex justify-content-center">
                            <i aria-hidden="true" class="fas fa-link align-self-center"></i>
                        </a>
                    </div>
                    <span class="share-label">Copy URL</span>
                </div>
                <hr />
            </div>

            <div class="side-title pl-4">
                <span class="table-row-height item-title d-block d-lg-none"></span>
                <span class="table-row-height item-description d-block"><a href="/">Major</a></span>
                <span class="table-row-height item-title d-block d-lg-none"></span>
                <span class="table-row-height item-description d-block"><a href="/">Course Level</a></span>
                <span class="table-row-height item-title d-block d-lg-none"></span>
                <span class="table-row-height item-description d-block"><a href="/">Nett fee</a></span>
                <span class="table-row-height-lg d-block"></span>
            </div>

            <div class="section-header text-center text-lg-left pl-lg-5 pt-1 font-semibold" id="cost-header" data-toggle="collapse" data-target=".cost-financial-section">
                Cost & Financial Aids <i class="fa fa-angle-up font-bold ml-2"></i>
            </div>

            <div class="collapse show cost-financial-section">
                <div id="ptptn-select">
                    <span class="d-block text-center section-title font-semibold pt-4">Select your PTPTN Amount</span>
                    <div class="text-center mt-3">
                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-primary active pr-4 pl-5">
                                <input type="radio" name="ptptn" id="0-pc" autocomplete="off" checked> 0%
                            </label>
                            <label class="btn btn-primary px-4">
                                <input type="radio" name="ptptn" id="50-pc" autocomplete="off"> 50%
                            </label>
                            <label class="btn btn-primary px-4">
                                <input type="radio" name="ptptn" id="75-pc" autocomplete="off"> 75%
                            </label>
                            <label class="btn btn-primary pl-4 pr-5">
                                <input type="radio" name="ptptn" id="100-pc" autocomplete="off"> 100%
                            </label>
                        </div>
                    </div>
                </div>

                <div class="side-title pl-4">
                    <span class="table-row-height item-title d-block d-lg-none"></span>
                    <span class="table-row-height item-description d-block"><a href="/">Scholarships</a></span>
                    <span class="table-row-height item-title d-block d-lg-none"></span>
                    <span class="table-row-height-lg item-description d-block"><a href="/">Instant Offers</a></span>
                    <span class="table-row-height item-title d-block d-lg-none"></span>
                    <span class="table-row-height item-description d-block"><a href="/">Course fee</a></span>
                    <span class="table-row-height item-title d-block d-lg-none"></span>
                    <span class="table-row-height item-description d-block"><a href="/">Nett Fee</a></span>
                    <span class="table-row-height item-title d-block d-lg-none"></span>
                    <span class="table-row-height item-description d-block"><a href="/">PTPTN Loan</a></span>
                    <span class="table-row-height item-title d-block d-lg-none"></span>
                    <span class="table-row-height item-description d-block"><a href="/">Final Commitment</a></span>
                </div>
            </div>

            <div class="section-header text-center text-lg-left pl-lg-5 pt-1 font-semibold" data-toggle="collapse" data-target=".course-section">
                Course <i class="fa fa-angle-up font-bold ml-2"></i>
            </div>

            <div class="side-title pl-4 collapse show course-section">
                <span class="table-row-height item-title d-block d-lg-none"></span>
                <span class="table-row-height item-description d-block"><a href="/">Duration</a></span>
                <span class="table-row-height item-title d-block d-lg-none"></span>
                <span class="table-row-height item-description d-block"><a href="/">Intakes</a></span>
                <span class="table-row-height item-title d-block d-lg-none"></span>
                <span class="table-row-height item-description d-block"><a href="/">Course Award</a></span>
                <span class="table-row-height item-title d-block d-lg-none"></span>
                <span class="table-row-height item-description d-block"><a href="/">Oversea Transfer</a></span>
            </div>

            <div class="section-header text-center text-lg-left pl-lg-5 pt-1 font-semibold" data-toggle="collapse" data-target=".institution-section">
                Institution <i class="fa fa-angle-up font-bold ml-2"></i>
            </div>

            <div class="side-title pl-4 collapse show institution-section">
                <span class="table-row-height item-title d-block d-lg-none"></span>
                <span class="table-row-height item-description d-block"><a href="/">Ranking</a></span>
                <span class="table-row-height item-title d-block d-lg-none"></span>
                <span class="table-row-height item-description d-block"><a href="/">Student Size</a></span>
                <span class="table-row-height item-title d-block d-lg-none"></span>
                <span class="table-row-height item-description d-block"><a href="/">International</a></span>
            </div>

            <div class="section-header text-center text-lg-left pl-lg-5 pt-1 font-semibold" data-toggle="collapse" data-target=".location-section">
                Location <i class="fa fa-angle-up font-bold ml-2"></i>
            </div>

            <div class="side-title pl-4 collapse show location-section">
                <span class="table-row-height item-title d-block d-lg-none"></span>
                <span class="table-row-height item-description d-block"><a href="/">Area</a></span>
                <span class="table-row-height item-title d-block d-lg-none"></span>
                <span class="table-row-height item-description d-block"><a href="/">Type</a></span>
                <span class="table-row-height item-title d-block d-lg-none"></span>
                <span class="table-row-height item-description d-block"><a href="/">Transport Availability</a></span>
            </div>
        </div>

        <div style="overflow: hidden" class="px-3 px-lg-5">
            <div class="owl-carousel" id="compare-carousel">
                @for ($i = 0; $i < 4; $i++)
                <div class="item px-2 px-sm-4 pt-2 pt-lg-4 pb-5 text-center">
                    <div class="top-container">
                        <button type="button" class="close-item" aria-label="Close" data-toggle="modal" data-index="{{ $i }}" data-target="#deleteCourseModal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="item-description text-left text-lg-center px-3" id="university">
                            Asia Pacific University of Technology & Innovation (APU)
                        </div>
                        <div class="item-img my-2 my-sm-4">
                            <img class="w-100 img-fluid" src="https://via.placeholder.com/263x100">
                        </div>
                        <div class="course-title mb-3 text-left pl-3">
                            <span>BSc (Hons) In Information Technology (3+0), Staffordshire University, UK ({{ $i+1 }})</span>
                        </div>
                    </div>
                    <span class="item-title table-row-height d-block d-lg-none">Major</span>
                    <span class="item-description table-row-height d-block">Information Technology</span>
                    <span class="item-title table-row-height d-block d-lg-none">Course Level</span>
                    <span class="item-description table-row-height d-block">Degree</span>
                    <span class="item-title table-row-height d-block d-lg-none">Nett Fee</span>
                    <span class="item-description-lg table-row-height d-block font-bold">MYR 76,600</span>
                    <div class="table-row-height-lg my-0">
                        <button class="btn text-center btn-primary font-semibold px-5 py-2">Apply</button>
                    </div>

                    <div class="section-header-space">
                    </div>
                    
                    <div class="collapse show cost-financial-section">
                        <div id="ptptn-select-space">
                        </div>

                        <span class="item-title table-row-height d-block d-lg-none">Scholarships</span>
                        <span class="item-description table-row-height d-block">Up to 100% from 4 scholarships available</span>
                        <span class="item-title table-row-height d-block d-lg-none">Instant Offers</span>
                        <span class="item-description table-row-height-lg d-block">Up to MYR 10,000 from 5 deals & merit scholarship</span>
                        <span class="item-title table-row-height d-block d-lg-none">Course Fee</span>
                        <span class="item-description table-row-height d-block">MYR 86,600 (MYR 28,867/year)</span>
                        <span class="item-title table-row-height d-block d-lg-none">Nett Fee</span>
                        <span class="item-description table-row-height d-block">MYR 76, 600 (based on potential offers)</span>
                        <span class="item-title table-row-height d-block d-lg-none">PTPTN Loan</span>
                        <span class="item-description table-row-height d-block">MYR <span class="ptptn-loan-amount">0 (PTPTN 0%)</span></span>
                        <span class="item-title table-row-height d-block d-lg-none">Final Commitment</span>
                        <span class="item-description-lg table-row-height d-block font-bold">MYR60,000 (MYR1,684/mth)</span>
                    </div>

                    <div class="section-header-space">
                    </div>

                    <div class="collapse show course-section">
                        <span class="item-title table-row-height d-block d-lg-none">Duration</span>
                        <span class="item-description table-row-height d-block">Information Technology</span>
                        <span class="item-title table-row-height d-block d-lg-none">Intakes</span>
                        <span class="item-description table-row-height d-block">Degree</span>
                        <span class="item-title table-row-height d-block d-lg-none">Course Award</span>
                        <span class="item-description table-row-height d-block">MYR 76,600</span>
                        <span class="item-title table-row-height d-block d-lg-none">Oversea Transfer</span>
                        <span class="item-description table-row-height d-block">Information Technology</span>
                    </div>

                    <div class="section-header-space">
                    </div>

                    <div class="collapse show institution-section">
                        <span class="item-title table-row-height d-block d-lg-none">Ranking</span>
                        <span class="item-description table-row-height d-block">SETARA Rating - Tier 5</span>
                        <span class="item-title table-row-height d-block d-lg-none">Student Size</span>
                        <span class="item-description table-row-height d-block">> 3000 students</span>
                        <span class="item-title table-row-height d-block d-lg-none">International</span>
                        <span class="item-description table-row-height d-block">5%)</span>
                    </div>

                    <div class="section-header-space">
                    </div>

                    <div class="collapse show location-section">
                        <span class="item-title table-row-height d-block d-lg-none">Area</span>
                        <span class="item-description table-row-height d-block">Bukit Jalil, Kuala Lumpur</span>
                        <span class="item-title table-row-height d-block d-lg-none">Type</span>
                        <span class="item-description table-row-height d-block">Urban</span>
                        <span class="item-title table-row-height d-block d-lg-none">Transport Availability</span>
                        <span class="item-description table-row-height d-block">By Rail, By Bus</span>
                    </div>
                </div>
                @endfor
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="deleteCourseModal" tabindex="-1" role="dialog" aria-labelledby="deleteCourseModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content px-3">
            <div class="modal-header pt-3">
                <h5 class="modal-title font-light" id="deleteCourseModalLabel">Confirm Removing The selected Courses?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <hr class="w-100 m-0" />
            <div class="modal-body pt-4">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-lg-5 border border-primary text-center py-3 my-4">
                            <span class="close-icon">&times;</span><br>
                            <span class="close-desc font-semibold">Remove Course from Comparison</span>
                        </div>
                    </div>
                </div>

                <p class="modal-desc text-center font-medium">This action cannot be undone</p>
            </div>
            <div class="modal-footer mb-5">
                <button type="button" class="btn btn-primary font-semibold px-5 py-2 confirm-remove">Remove</button>
            </div>
        </div>
    </div>
</div>
@endsection