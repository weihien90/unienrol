@extends('layouts.app')

@section('custom_css')
    <link href="{{ url('css/contact-us/contact-us.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('custom_js')
    <script src="{{ url('js/contact-us/contact-us.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
<section class="contact-us details my-4">
    <div class="container">
        <div class="row my-3">
            <div class="col-12">
                <span class="section-title font-light d-block">Get in touch with us today</span>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 order-2 order-md-1 mt-3 mt-md-0">
                <dl>
                    <dt class="font-medium">Our Address</dt>
                    <dd class="font-medium mb-4 mb-md-5">B-03-08, Sunway Geo Avenue, Jalan Lagoon Selatan, Bandar Sunway 47500 Subang Jaya, Selangor.</dd>
                    <dt class="font-medium">Opening Hours</dt>
                    <dd class="font-medium mb-4 mb-md-5">Mon-Sun <br>10:00 am - 6:00am</dd>
                    <dt class="font-medium">Call us</dt>
                    <dd class="font-medium mb-4 mb-md-5">03-5613 7885</dd>
                    <dt class="font-medium">Email Address</dt>
                    <dd class="font-medium mb-4 mb-md-5">sampleemail@unienrol.com</dd>
                </dl>
            </div>

            <div class="col-md-7 order-1 order-md-2">
                <div id="map" class="img-fluid float-md-right"></div>
            </div>
        </div>
    </div>
</section>

<section class="contact-us contact-form my-4">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav>
                    <div class="nav nav-tabs px-md-3 mb-3 font-medium flex-nowrap" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active show mr-5" id="students-parents-tab" data-toggle="tab" href="#nav-students-parents" role="tab" aria-controls="nav-students-parents" aria-selected="true">
                            Students / Parents
                        </a>
                        <a class="nav-item nav-link" id="partners-tab" data-toggle="tab" href="#nav-partners" role="tab" aria-controls="nav-partners" aria-selected="true">
                            Partner with Unienrol
                        </a>
                    </div>
                </nav>

                <div class="tab-content px-md-3" id="nav-tabContent">
                    <div class="tab-pane fade active show" id="nav-students-parents" role="tabpanel" aria-labelledby="students-parents-tab">
                        <span class="section-description font-medium">
                            Short description / example on what they should ask Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis 
                            aute irure dolor in reprehenderit in voluptate velit esse ci
                        </span>
                        <div class="form-row mt-4">
                            <div class="col-md-6 col-xl-5 mb-3">
                                <div class="text-input-container">
                                    <label for="sp-name" class="font-medium">Name*</label>
                                    <input name="name" id="sp-name" class="form-control py-4" placeholder="John Doe" />
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-5 mb-3">
                                <div class="text-input-container">
                                    <label for="sp-email" class="font-medium">Email*</label>
                                    <input name="email" id="sp-email" class="form-control py-4" placeholder="johndoe@gmail.com" />
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-5 mb-3">
                                <div class="text-input-container">
                                    <label for="sp-phone" class="font-medium">Phone Number*</label>
                                    <input name="phone" id="sp-phone" class="form-control py-4" placeholder="010 12345678" />
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-5 mb-3">
                                <div class="select-input-container">
                                    <label for="sp-time" class="font-medium">Preferred timing for Consultation Call</label>
                                    <select name="time" id="sp-time" class="form-control">
                                        <option>9am - 10am</option>
                                        <option>10am - 11am</option>
                                        <option>11am - 12pm</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-10 mx-2">
                                <span class="file-desc font-medium d-block mt-2 mt-md-0">Snap a photo / Scan your result transcript</span>
                                <div class=" text-center float-md-right">
                                    <label for="sp-file" class="btn btn-primary font-semibold px-5 py-2 mt-2 mt-md-0">BROWSE YOUR FILE</label>
                                </div>
                                <span class="file-desc-small font-medium">Acceptable format : jpeg/pdf/png , not bigger than 2mb</span>
                                <input type="file" name="file" id="sp-file" />
                                <div class="font-medium mt-2 d-block invisible" id="selected-file-container">
                                    <span id="selected-file"></span>
                                    <i id="remove-file" class="pl-1 fa fa-times"></i>
                                </div>
                            </div>

                            <div class="col-xl-10 mb-3 mt-2">
                                <textarea class="form-control p-3" placeholder="Your Message Here"></textarea>
                            </div>

                            <div class="col-xl-10 mt-3 d-flex flex-wrap justify-content-md-between justify-content-center align-items-center">
                                <div class="g-recaptcha" data-sitekey="6LeUbHsUAAAAADgU-cvzorA-Ug_pWkOafFOfu3uT"></div>
                                <button class="btn btn-primary font-semibold px-5 py-2 mt-3 mt-md-0" type="submit">SUBMIT YOUR MESSAGE</button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-partners" role="tabpanel" aria-labelledby="partners-tab">
                        <span class="section-description font-medium">
                            Wherther you're Agents, Scholarship & foundation or Corporates, we are open for collaborations.... short description / example on what they should ask Lorem ipsum 
                            dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
                            ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse ci
                        </span>
                        <div class="form-row mt-4">
                            <div class="col-md-6 col-xl-5 mb-3">
                                <div class="text-input-container">
                                    <label class="font-medium">Name*</label>
                                    <input class="form-control py-4" placeholder="John Doe" />
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-5 mb-3">
                                <div class="text-input-container">
                                    <label class="font-medium">Email*</label>
                                    <input class="form-control py-4" placeholder="johndoe@gmail.com" />
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-5 mb-3">
                                <div class="text-input-container">
                                    <label class="font-medium">Phone Number*</label>
                                    <input class="form-control py-4" placeholder="010 12345678" />
                                </div>
                            </div>
                            <div class="col-xl-10 mb-5">
                                <textarea class="form-control p-3" placeholder="Your Message Here"></textarea>
                            </div>
                            <div class="col-xl-10 mt-3 d-flex flex-wrap justify-content-md-between justify-content-center align-items-center">
                                <div class="g-recaptcha" data-sitekey="6LeUbHsUAAAAADgU-cvzorA-Ug_pWkOafFOfu3uT"></div>
                                <button class="btn btn-primary font-semibold px-5 py-2 mt-3 mt-md-0" type="submit">SUBMIT YOUR MESSAGE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    function initMap() {
        var unienrolLocation = {lat: 3.0647762, lng: 101.6097834};
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 17, center: unienrolLocation});
        var marker = new google.maps.Marker({position: unienrolLocation, map: map});
    }
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWaOsfjtQSSH9AB-EM7StjrHsZ8pMDcSA&callback=initMap"></script>
@endsection