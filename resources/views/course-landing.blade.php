@extends('layouts.app')

@section('custom_css')
    <link href="{{ url('css/course-landing/course-landing.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<section class="courses header">
    <div class="container full-height">
        <div class="row py-3 py-md-5">
            <div class="col-12 mt-1">
                <span class="header-title font-semibold d-block">Getting The Right Courses For Yourself</span>
                <span class="header-desc d-block pl-md-2">Explore and understand the majority of courses out there</span>
            </div>
        </div>
    </div>
</section>

<section class="courses fields mt-4">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav>
                    <div class="nav nav-tabs px-3 mb-3" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active show" id="nav-fields-tab" data-toggle="tab" href="#nav-fields" role="tab" aria-controls="nav-preU" aria-selected="true">Field of Study</a>
                    </div>
                </nav>

                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade active show" id="nav-fields" role="tabpanel" aria-labelledby="nav-fields-tab">
                        <div class="row no-gutters">
                            <div class="col-6 col-lg-3 mb-2 mb-md-3 px-1 px-md-3">
                                <a href="https://unienrol.com/search">
                                    <div class="tiles-container">
                                        <div class="tiles-bg-overlay"></div>
                                        <img class="tiles-img img-fluid w-100" src="{{ url('img/course-landing/bg_accounting.jpg') }}" />
                                        <div class="tiles-title font-medium text-center">Accounting</div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-6 col-lg-3 mb-2 mb-md-3 px-1 px-md-3">
                                <a href="https://unienrol.com/search">
                                    <div class="tiles-container">
                                        <div class="tiles-bg-overlay"></div>
                                        <img class="tiles-img img-fluid w-100" src="{{ url('img/course-landing/bg_architecture.jpg') }}" />
                                        <div class="tiles-title font-medium text-center">Architecture & Built Environment</div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-6 col-lg-3 mb-2 mb-md-3 px-1 px-md-3">
                                <a href="https://unienrol.com/search">
                                    <div class="tiles-container">
                                        <div class="tiles-bg-overlay"></div>
                                        <img class="tiles-img img-fluid w-100" src="{{ url('img/course-landing/bg_business_finance.jpg') }}" />
                                        <div class="tiles-title font-medium text-center">Business & Finance</div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-6 col-lg-3 mb-2 mb-md-3 px-1 px-md-3">
                                <a href="https://unienrol.com/search">
                                    <div class="tiles-container">
                                        <div class="tiles-bg-overlay"></div>
                                        <img class="tiles-img img-fluid w-100" src="{{ url('img/course-landing/bg_communication_media_studies.jpg') }}" />
                                        <div class="tiles-title font-medium text-center">Communications & Media Studies</div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-6 col-lg-3 mb-2 mb-md-3 px-1 px-md-3">
                                <a href="https://unienrol.com/search">
                                    <div class="tiles-container">
                                        <div class="tiles-bg-overlay"></div>
                                        <img class="tiles-img img-fluid w-100" src="{{ url('img/course-landing/bg_computer_it.jpg') }}" />
                                        <div class="tiles-title font-medium text-center">Computing & IT</div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-6 col-lg-3 mb-2 mb-md-3 px-1 px-md-3">
                                <a href="https://unienrol.com/search">
                                    <div class="tiles-container">
                                        <div class="tiles-bg-overlay"></div>
                                        <img class="tiles-img img-fluid w-100" src="{{ url('img/course-landing/bg_education.jpg') }}" />
                                        <div class="tiles-title font-medium text-center">Education</div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-6 col-lg-3 mb-2 mb-md-3 px-1 px-md-3">
                                <a href="https://unienrol.com/search">
                                    <div class="tiles-container">
                                        <div class="tiles-bg-overlay"></div>
                                        <img class="tiles-img img-fluid w-100" src="{{ url('img/course-landing/bg_engineering.jpg') }}" />
                                        <div class="tiles-title font-medium text-center">Engineering</div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-6 col-lg-3 mb-2 mb-md-3 px-1 px-md-3">
                                <a href="https://unienrol.com/search">
                                    <div class="tiles-container">
                                        <div class="tiles-bg-overlay"></div>
                                        <img class="tiles-img img-fluid w-100" src="{{ url('img/course-landing/bg_law.jpg') }}" />
                                        <div class="tiles-title font-medium text-center">Law</div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-6 col-lg-3 mb-2 mb-md-3 px-1 px-md-3">
                                <a href="https://unienrol.com/search">
                                    <div class="tiles-container">
                                        <div class="tiles-bg-overlay"></div>
                                        <img class="tiles-img img-fluid w-100" src="{{ url('img/course-landing/bg_medicines_allied_health_sciences.jpg') }}" />
                                        <div class="tiles-title font-medium text-center">Medicine & Allied Health Sciences</div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-6 col-lg-3 mb-2 mb-md-3 px-1 px-md-3">
                                <a href="https://unienrol.com/search">
                                    <div class="tiles-container">
                                        <div class="tiles-bg-overlay"></div>
                                        <img class="tiles-img img-fluid w-100" src="{{ url('img/course-landing/bg_pre_u.jpg') }}" />
                                        <div class="tiles-title font-medium text-center">Pre-University</div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-6 col-lg-3 mb-2 mb-md-3 px-1 px-md-3">
                                <a href="https://unienrol.com/search">
                                    <div class="tiles-container">
                                        <div class="tiles-bg-overlay"></div>
                                        <img class="tiles-img img-fluid w-100" src="{{ url('img/course-landing/bg_sciences_mathematics.jpg') }}" />
                                        <div class="tiles-title font-medium text-center">Science & Mathematics</div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-6 col-lg-3 mb-2 mb-md-3 px-1 px-md-3">
                                <a href="https://unienrol.com/search">
                                    <div class="tiles-container">
                                        <div class="tiles-bg-overlay"></div>
                                        <img class="tiles-img img-fluid w-100" src="{{ url('img/course-landing/bg_language_social_studies.jpg') }}" />
                                        <div class="tiles-title font-medium text-center">Language & Social Studies</div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-6 col-lg-3 mb-2 mb-md-3 px-1 px-md-3">
                                <a href="https://unienrol.com/search">
                                    <div class="tiles-container">
                                        <div class="tiles-bg-overlay"></div>
                                        <img class="tiles-img img-fluid w-100" src="{{ url('img/course-landing/bg_hospitality_services.jpg') }}" />
                                        <div class="tiles-title font-medium text-center">Hospitality & Services</div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-6 col-lg-3 mb-2 mb-md-3 px-1 px-md-3">
                                <a href="https://unienrol.com/search">
                                    <div class="tiles-container">
                                        <div class="tiles-bg-overlay"></div>
                                        <img class="tiles-img img-fluid w-100" src="{{ url('img/course-landing/bg_creative_arts.jpg') }}" />
                                        <div class="tiles-title font-medium text-center">Creative Arts & Design</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="courses contact my-5">
    <div class="container">
        <div class="row justify-content-center">
            <span class="section-description font-light mb-4 px-2 text-center">
                Can't find the course you want? Contact our counsellor for help!
            </span>
            <div class="col-12 text-center">
                <button class="btn btn-primary px-5 py-2 font-semibold">CONTACT COUNSELLOR</button>
            </div>
        </div>
    </div>
</section>
@endsection