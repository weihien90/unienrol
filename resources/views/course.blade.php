@extends('layouts.app')

@section('custom_css')
    <link href="{{ url('css/course/course.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('custom_js')
    <script src="{{ url('js/course/course.js') }}"></script>
@endsection

@section('content')
<section class="course header">
    <div class="container full-height">
        <div class="row py-3 py-md-5">
            <div class="col-12">
                <span class="header-title font-semibold d-block">BUSINESS & FINANCE</span>
                <span class="header-desc d-block mb-3">
                    Accounting involves recording, analysing and reporting financial transactions in order for companies to be able to make strategic 
                    business decisions. Accounting can be divided into several fields including financial accounting, management accounting, external 
                    auditing, tax accounting and cost accounting. As an accountant, you will be responsible for an individual’s or a company’s financial 
                    statements, ensuring that they are accurate and comply with laws and regulations. Accountants advise their clients or employers on 
                    ways to reduce costs and improve profits. Accountants also need to analyze trends to project future revenues and expenses.
                </span>
                <span class="header-small d-block font-semibold pull-right">See Other Courses ></span>
            </div>
        </div>
    </div>
</section>

<section class="course majors my-4">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <span class="section-title font-semibold text-center">Majors in Business & Finance</span>
            </div>
            <div class="hr col-12 mt-2 mb-3"></div>
            <div class="col-12 text-center mb-4">
                <span class="section-sub-title text-center">Click on a major to see the available courses</span>
            </div>
            <div class="col-12 px-md-5 mx-md-4">
            @for ($i = 0; $i < 20; $i++)
                <button class="btn px-3 px-md-4 mr-2 mb-2 mb-md-3 py-1 font-medium btn-majors-tag">
                    {{ str_limit('Logistic & Supply Chain Management', rand(10, 34), '') }}
                </button>
            @endfor
            </div>
        </div>

        <div class="accordion my-3 my-md-5" id="majorsAccordion">
            <div class="card mt-2">
                <div class="card-header px-0 px-md-3 py-1" data-toggle="collapse" data-target="#actuarialScience">
                    <h5 class="mb-0">
                        <button class="btn btn-link font-medium d-flex align-items-center" type="button" aria-expanded="true" aria-controls="actuarialScience">
                            <i class="fas fa-angle-right mr-3 fa-2x"></i> Actuarial Science
                        </button>
                    </h5>
                </div>
                <div id="actuarialScience" class="collapse" data-parent="#majorsAccordion">
                    <div class="card-body pt-2 pb-4 pl-2 pl-md-5 ml-3">
                        <a href="#">See All Courses</a><br>
                        <p class="mt-2">Have you ever wondered how overpasses that cross over other roads are conceptualized or who designed the tallest building the world? Studying architecture 
                        will allow you to imagine, design and construct the streets and buildings of the future. This guide will take you through everything you need to know about 
                        studying an architecture degree in Malaysia, and it is not limited to the way buildings and structures look, but concerns the needs of the people who will 
                        use them. Have you ever wondered how overpasses that cross over other roads are conceptualized or who designed the tallest building the world? Studying 
                        architecture will allow you to imagine, design and construct the streets and buildings of the future. This guide will take you through everything you need 
                        to know about studying an architecture</p>
                    </div>
                </div>
            </div>

            <div class="card mt-2">
                <div class="card-header px-0 px-md-3 py-1" data-toggle="collapse" data-target="#bankingFinance">
                    <h5 class="mb-0">
                        <button class="btn btn-link font-medium d-flex align-items-center" type="button" aria-expanded="true" aria-controls="bankingFinance">
                            <i class="fas fa-angle-right mr-3 fa-2x"></i> Banking & finance
                        </button>
                    </h5>
                </div>
                <div id="bankingFinance" class="collapse" data-parent="#majorsAccordion">
                    <div class="card-body pt-2 pb-4 pl-2 pl-md-5 ml-3">
                        <a href="#">See All Courses</a><br>
                        <p class="mt-2">Have you ever wondered how overpasses that cross over other roads are conceptualized or who designed the tallest building the world? Studying architecture 
                        will allow you to imagine, design and construct the streets and buildings of the future. This guide will take you through everything you need to know about 
                        studying an architecture degree in Malaysia, and it is not limited to the way buildings and structures look, but concerns the needs of the people who will 
                        use them. Have you ever wondered how overpasses that cross over other roads are conceptualized or who designed the tallest building the world? Studying 
                        architecture will allow you to imagine, design and construct the streets and buildings of the future. This guide will take you through everything you need 
                        to know about studying an architecture</p>
                    </div>
                </div>
            </div>

            <div class="card mt-2">
                <div class="card-header px-0 px-md-3 py-1" data-toggle="collapse" data-target="#businessAdministration">
                    <h5 class="mb-0">
                        <button class="btn btn-link font-medium d-flex align-items-center" type="button" aria-expanded="true" aria-controls="businessAdministration">
                            <i class="fas fa-angle-right mr-3 fa-2x"></i> Business Administration
                        </button>
                    </h5>
                </div>
                <div id="businessAdministration" class="collapse" data-parent="#majorsAccordion">
                    <div class="card-body pt-2 pb-4 pl-2 pl-md-5 ml-3">
                        <a href="#">See All Courses</a><br>
                        <p class="mt-2">Have you ever wondered how overpasses that cross over other roads are conceptualized or who designed the tallest building the world? Studying architecture 
                        will allow you to imagine, design and construct the streets and buildings of the future. This guide will take you through everything you need to know about 
                        studying an architecture degree in Malaysia, and it is not limited to the way buildings and structures look, but concerns the needs of the people who will 
                        use them. Have you ever wondered how overpasses that cross over other roads are conceptualized or who designed the tallest building the world? Studying 
                        architecture will allow you to imagine, design and construct the streets and buildings of the future. This guide will take you through everything you need 
                        to know about studying an architecture</p>
                    </div>
                </div>
            </div>

            <div class="card mt-2">
                <div class="card-header px-0 px-md-3 py-1" data-toggle="collapse" data-target="#economics">
                    <h5 class="mb-0">
                        <button class="btn btn-link font-medium d-flex align-items-center" type="button" aria-expanded="true" aria-controls="economics">
                            <i class="fas fa-angle-right mr-3 fa-2x"></i> Economics
                        </button>
                    </h5>
                </div>
                <div id="economics" class="collapse" data-parent="#majorsAccordion">
                    <div class="card-body pt-2 pb-4 pl-2 pl-md-5 ml-3">
                        <a href="#">See All Courses</a><br>
                        <p class="mt-2">Have you ever wondered how overpasses that cross over other roads are conceptualized or who designed the tallest building the world? Studying architecture 
                        will allow you to imagine, design and construct the streets and buildings of the future. This guide will take you through everything you need to know about 
                        studying an architecture degree in Malaysia, and it is not limited to the way buildings and structures look, but concerns the needs of the people who will 
                        use them. Have you ever wondered how overpasses that cross over other roads are conceptualized or who designed the tallest building the world? Studying 
                        architecture will allow you to imagine, design and construct the streets and buildings of the future. This guide will take you through everything you need 
                        to know about studying an architecture</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="course tiles mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-2">
                <span class="section-title font-light">Career opportunity after Graduating</span>
            </div>
            <div class="hr col-12 mt-3"></div>
            <div class="owl-carousel" id="opportunities-carousel">
                @for ($i = 0; $i < 4; $i++)                
                <div class="col-12 item my-3 my-lg-4 px-md-4 text-center">
                    <div class="article-img mb-3">
                        <img class="w-100 img-fluid" src="https://via.placeholder.com/291x141">
                    </div>
                    <div class="article-title font-light mb-3">
                        <span>Career Title</span>
                    </div>
                    <div class="article-desc">
                        <span>Subtitle Content</span>
                    </div>
                </div>
                @endfor
            </div>
        </div>
    </div>
</section>

<section class="course tiles mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-2">
                <span class="section-title font-light">Universities to Consider</span>
            </div>
            <div class="hr col-12 mt-3"></div>
            <div class="owl-carousel" id="universities-carousel">
                @for ($i = 0; $i < 4; $i++)                
                <div class="col-12 item my-3 my-lg-4 px-md-4 text-center">
                    <div class="article-img mb-3">
                        <img class="w-100 img-fluid" src="https://via.placeholder.com/291x204">
                    </div>
                    <div class="article-title font-light mb-3">
                        <span>Sunway Medical</span>
                    </div>
                    <div class="article-desc">
                        <span>Subtitle Content</span>
                    </div>
                </div>
                @endfor
            </div>
        </div>
    </div>
</section>

<section class="course tiles my-5">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-2">
                <span class="section-title font-light">Other articles you might be interested in</span>
            </div>
            <div class="hr col-12 mt-3"></div>
            @for ($i = 0; $i < 4; $i++)                
            <div class="col-md-6 col-lg-3 my-3 my-lg-4 px-md-4">
                <div class="article-img mb-3">
                    <img class="w-100 img-fluid" src="https://via.placeholder.com/277x161">
                </div>
                <div class="article-title font-light mb-3">
                    <span>What Do You Get From a Full Scholarship at LIMKOKWING University</span>
                </div>
                <div class="article-desc">
                    <span>An amazing experience awaits you at Limkokwing University</span>
                </div>
            </div>
            @endfor
        </div>
    </div>
</section>
@endsection