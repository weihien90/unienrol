@extends('layouts.app')

@section('custom_css')
    <link href="{{ url('css/guides/guides.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('custom_js')
    <script src="{{ url('js/guides/guides.js') }}"></script>
@endsection

@section('content')
<section class="guides header" style="background-image: url('https://via.placeholder.com/1961x202')">
    <div class="bg-overlay"></div>
    <div class="container full-height">
        <div class="row pt-2 pb-4 py-md-5">
            <div class="col-12 mt-1">
                <span class="header-title font-semibold d-block">Quick Guides</span>
                <span class="header-desc d-block pl-md-2">Our quick guides help you to navigate through the hassle process of getting a higher education</span>
            </div>
        </div>
    </div>
</section>

<section class="guides categories my-3 my-md-5">
    <div class="container">
        <div class="row px-md-5">
            <div class="col-12">
            @for ($i = 0; $i < 6; $i++)
                <button class="btn px-3 px-md-4 mr-2 mb-2 mb-md-3 py-1 font-medium btn-categories">
                    {{ str_limit('Scholarships & Guides', rand(10, 21), '') }}
                </button>
            @endfor
            </div>
        </div>
    </div>
</section>

<section class="guides featured-articles pt-2">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <span class="section-title d-block font-bold mb-2">Featured Articles</span>
            </div>

            @for ($i = 0; $i < 5; $i++)
            <div class="col-lg-6 mb-2">
                <a href="/">
                    <div class="bg-container pt-5 px-2 px-md-4 h-100 position-relative">
                        <div class="bg-overlay"></div>
                        <div class="row pt-5">
                            <div class="col-12 mb-3">
                                <span class="header-tag font-medium py-1 pl-2 pr-4">Quick Guides</span>
                            </div>
                            <div class="col-12 mb-3">
                                <span class="header-title font-semibold mb-2">Article Short title</span>
                                <p class="header-desc font-medium mb-1">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                </p>
                                <span class="header-desc-small d-block">Cheng Sim - August 24, 2018</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endfor
        </div>
    </div>
</section>

<section class="guides latest-articles mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="section-title d-block font-bold mb-2">Latest Articles</span>
            </div>

            @for ($i = 0; $i < 4; $i++)
            <div class="col-lg-6 border-bottom">
                <a href="/">
                    <div class="d-flex h-100 flex-nowrap align-items-center py-3">
                        <div>
                            <img class="article-img" src="https://via.placeholder.com/76x89">
                        </div>
                        <div class="flex-grow-1 ml-2">
                            <span class="article-tag font-medium pl-2 pr-3">Category Name</span>
                            <p class="article-title font-semibold mb-0">Lorem ipsum dolor sit amet</p>
                            <p class="article-desc font-light mb-1">
                                consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                            </p>
                            <span class="article-desc-small d-block">Cheng Sim - August 24, 2018</span>
                        </div>
                    </div>
                </a>
            </div>
            @endfor

            <template v-for="article in articles">
            <div class="col-lg-6 border-bottom">
                <a :href="article.url">
                    <div class="d-flex flex-nowrap align-items-center py-3">
                        <div>
                            <img class="article-img" :src="article.image">
                        </div>
                        <div class="flex-grow-1 ml-2">
                            <span class="article-tag font-medium pl-2 pr-3">@{{ article.category_name }}</span>
                            <p class="article-title font-semibold mb-0">@{{ article.name }}</p>
                            <p class="article-desc font-light mb-1">
                                @{{ article.description }}
                            </p>
                            <span class="article-desc-small d-block">@{{ article.author }} - @{{ article.publish_date_human }}</span>
                        </div>
                    </div>
                </a>
            </div>
            </template>

            <div class="col-12 mt-5">
                <span class="d-block text-center font-medium mb-2 more-articles">
                    <i v-if="loading" class="fas fa-spin fa-spinner font-bold"></i>
                    <a v-else="loading" href="#" @click.prevent="loadMore">More Articles <i class="fa fa-angle-down font-bold"></i></a>
                </span>
            </div>
        </div>
    </div>
</section>

<section class="guides cta my-5">
    <div class="container">
        <div class="row justify-content-center">
            <span class="section-description font-medium mb-3 px-2 text-center">
                Need more guidance?
            </span>
            <div class="col-12 text-center">
                <button class="btn btn-primary px-5 py-2 font-semibold">CHAT WITH OUR COUNSELLOR</button>
            </div>
        </div>
    </div>
</section>
@endsection