<!DOCTYPE html>
<html lang="en">
<head>
    <title>Uni Enrol | TITLE</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <!-- Bootstrap css and Font Family Css -->
    <link href="https://unienrol.com/frontend/css/font.css?v=1.9" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link href="https://unienrol.com/frontend/css/bootstrap.css?v=1.9" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?v=1.9" rel="stylesheet" />
    
    
    <!-- OwlCarousel Plugin Css -->
    <link href="https://unienrol.com/frontend/css/OwlCarousel/owl.carousel.min.css" rel="stylesheet" />
    <link href="https://unienrol.com/frontend/css/OwlCarousel/owl.theme.default.min.css" rel="stylesheet" />
    
    <!-- Select2 Plugin Css -->
    <link href="https://unienrol.com/frontend/css/select2.min.css" rel="stylesheet" />
    
    <!-- Bootstrap MultiSelect Css-->
    <link href="https://unienrol.com/frontend/css/bootstrap-multiselect.css" rel="stylesheet" />
    <link href="https://unienrol.com/frontend/css/styling.css?v=1.9" rel="stylesheet" />

    <!-- Custom Shared CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="{{ url('css/login/login.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('css/header/header.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('css/survey/survey.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Custom Page-specific CSS -->
    @section('custom_css')
    @show
    
    <!-- Jquery CDN -->
    <script src="https://unienrol.com/frontend/js/jquery.min.js"></script>
    <script src="https://unienrol.com/frontend/js/bootstrap.bundle.min.js"></script>
    
    <!-- OwlCarousel Plugin Javascript file -->
    <script src="https://unienrol.com/frontend/js/OwlCarousel/owl.carousel.min.js"></script>
    
    <!-- Select2 Plugin Javascript File -->
    <script src="https://unienrol.com/frontend/js/select2.min.js"></script>
    
    <!-- Bootstrap MultiSelect -->
    <script src="https://unienrol.com/frontend/js/bootstrap-multiselect.js"></script>

    <!-- Bootstrap Typeahead -->
    <script src="https://unienrol.com/frontend/js/typeahead.bundle.min.js"></script>

     <!-- Vue.js -->
        <script src="https://unienrol.com/frontend/js/vue.min.js"></script>
    <script src="https://unienrol.com/frontend/js/vue-resource.min.js"></script>

    <!-- Custom Javascript function -->
    <script src="https://unienrol.com/frontend/js/function.js?v=1.9"></script>

    <!-- Custom Shared JS -->
    <script src="{{ url('js/login/login.js') }}"></script>
    <script src="{{ url('js/header/header.js') }}"></script>
    <script src="{{ url('js/survey/survey.js') }}"></script>

    <!-- Custom Page-specific JS -->
    @section('custom_js')
    @show

    <script>
        var UE = {stash:{}};
        UE.session = {'loggedIn': false,'user': null};
    </script>
</head>

<body>
    @include('layouts/navbar')

    <!-- Content goes here -->
    <!-- This section can be rewritten to fit the content mockup accordingly -->
    @yield('content')
    <!-- Content ends here -->

    @include('layouts.footer')

    @include('layouts/login')
    @include('layouts/survey')
</body>
</html>
