    <footer>
        <div class="container pt-4">
            <div class="row">
                <div class="col-12">
                    <hr/>
                </div>
            </div>
    
            <div class="row">
                <div class="col-6 col-md-3">
                    <div class="footer-menu">
                        <div class="footer-menu-title">
                            <span>Uni Enrol</span>
                        </div>
    
                        <ul>
                            <li><a href="https://unienrol.com/about-us">About Us</a></li>
                            <li><a href="https://unienrol.com/articles">Articles</a></li>
                            <li><a href="https://unienrol.com/terms">Policies</a></li>
                            <li><a href="https://unienrol.com/join-us">Careers at Uni Enrol</a></li>
                            <li><a href="https://unienrol.com/contact-us">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
    
                <div class="col-6 col-md-3">
                    <div class="footer-menu">
                        <div class="footer-menu-title">
                            <span>Discover</span>
                        </div>
    
                        <ul>
                            <li><a href="https://unienrol.com/search/pathwaycheck">Pathway Check</a></li>
                            <li><a href="https://unienrol.com/compare">Compare</a></li>
                            <li><a href="https://unienrol.com/profile/application">Apply</a></li>
                            <li><a href="https://unienrol.com/guide">FAQs</a></li>
                        </ul>
                    </div>
                </div>
    
                <div class="col-6 col-md-3">
                    <div class="footer-menu">
                        <div class="footer-menu-title">
                            <span>Find Us</span>
                        </div>
                        <ul>
                            <li class="text-white">B-03-08, Sunway Geo Avenue <br>
                                47500 Subang Jaya, Selangor <br>
                                +603 5613 7225</li>
                        </ul>
                    </div>
                </div>
    
                <div class="col-12 col-md-3 text-left text-md-right">
                    <img src="https://unienrol.com/frontend/images/logos/inverse/UE_logo_white.svg" width="200px"/>
                </div>
            </div>
    
            <div class="row">
                <div class="col-12 my-3">
                    <span class="copyright">© 2018 Uni Enrol Sdn. Bhd. | Terms and Conditions </span>
                </div>
            </div>
        </div>
    </footer>