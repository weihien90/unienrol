<div id="loginlink">
    <login-modal id="login-modal" v-bind:modal_type="modal_type" v-if="showModal" @close="showModal = false"></login-modal>
</div>

<script type="text/x-template" id="login-modal-template">
  <transition name="modal">
    <div class="modal-mask">
      <div class="modal-wrapper" @click="close">
        <div class="modal-container" @click.stop>
          
          <div class="modal-body my-3 py-sm-4">
            <slot name="body">
              <!-- Login form -->
	            <div id="login-form">
                <span class="section-title mb-4 ml-2 upper font-medium">Login</span>
                <button type="button" class="close float-right d-sm-none" aria-label="Close" @click="close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <hr />
	              <form @submit.prevent="submitLogin" class="form-horizontal" action="https://unienrol.com/auth/login" method="POST">
	                <input type="hidden" name="_token" value="miDP6RM1b8z32plAJAZox0oq8AQHtBeCvKhlrMOh">
                  	                <slot name="modal_type2"></slot>
	                <div class="social-icon mb-2 d-flex justify-content-center">
		                <a class="btn btn-lg btn-facebook" href="https://unienrol.com/facebook/redirect"><i class="fa fa-2x fa-facebook" aria-hidden="true"></i></a>
		                <a class="btn btn-lg btn-google" href="https://unienrol.com/google/redirect"><i class="fa fa-2x fa-google-plus-g" aria-hidden="true"></i></a>
		                <a class="btn btn-lg btn-linkedin" href="https://unienrol.com/linkedin/redirect"><i class="fa fa-2x fa-linkedin" aria-hidden="true"></i></a>
		              </div>
		              <div style="margin-top: 16px; margin-bottom: 16px;">
		                <div class="breakline_1">
		                <span class="breakline_2">
		                  <span class="breakline_3">
		                    <span class="font-medium">OR</span>
		                  </span>
		                </span>
	                </div>
	              </div>
                <div class="row">
                  <div class="col-12 mb-2">
                    <div class="text-input-container">
                      <label for="login-email" class="font-medium">Email</label>
                      <input v-model="formInputs.email" type="email" name="email" id="login-email" class="form-control py-4" placeholder="johndoe@email.com" value="" />
                    </div>
                    <span v-if="formErrors['email']" class="error" v-text="formErrors['email'][0]"></span>
                  </div>
                  <div class="col-12 mb-2">
                    <div class="text-input-container">
                      <label for="login-password" class="font-medium">Password</label>
                      <input v-model="formInputs.password" name="password" type="password" id="login-password" class="form-control py-4" placeholder="************" />
                    </div>
                    <span v-if="formErrors['email']" class="error" v-text="formErrors['email'][0]"></span>
                  </div>

                  <div class="col-sm-12 mt-2">
                    <div class="form-group px-2">
                      <label class="remember-me font-medium">
                        <input v-model="formInputs.remember" type="checkbox" name="remember"> Remember Me
                      </label>
                      <div class="pull-right forget-password font-medium">
                        <a href="https://unienrol.com/password/reset" class="text-primary" onclick="userActivities('click','login','forgot password','https://unienrol.com/password/reset')">Forget Password</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 text-center text-primary">
                    <div class="login-footer" style="display:none;">
                      <a class="login-acct-link text-primary font-medium" @click.self="openLogin">Already have an Unienrol account? Log In</a>
                    </div>
                    <div class="signup-footer">
                      <a class="create-acct-link text-primary font-medium" @click.self="openSignup">Not a member yet? Create your account</a>
                    </div>
                  </div>
                  <div class="text-center col-sm-12 mt-2" id="tnc-signup">
                    <small class="section-desc font-light">
                      By clicking Sign-up or Login, I agree to Uni Enrol’s <a href="https://unienrol.com/terms">Terms of Service, and Privacy Policy.</a>
                    </small><br><br>
                  </div>
                  <div class="col-sm-12 text-center">
                    <button class="btn btn-primary font-semibold" type="submit">
                      <i class="fa fa-spinner fa-spin login-spinner" style="display:none;"></i> LOGIN
                    </button>
                  </div>
                </div>
              </form>
            </div>

            <!-- Signup form -->
            <div id="signup-form" style="display:none;">
              <form @submit.prevent="submitSignup" class="form-horizontal" action="https://unienrol.com/auth/register" method="POST">
              <input type="hidden" name="_token" value="miDP6RM1b8z32plAJAZox0oq8AQHtBeCvKhlrMOh">
              <slot name="modal_type"></slot>
              <span class="section-title mb-4 ml-2 upper font-medium">Sign Up</span>
              <button type="button" class="close float-right d-sm-none" aria-label="Close" @click="close">
                <span aria-hidden="true">&times;</span>
              </button>
              <hr />
              <div class="social-icon mb-2 d-flex justify-content-center">
		                <a class="btn btn-lg btn-facebook" href="https://unienrol.com/facebook/redirect"><i class="fa fa-2x fa-facebook" aria-hidden="true"></i></a>
		                <a class="btn btn-lg btn-google" href="https://unienrol.com/google/redirect"><i class="fa fa-2x fa-google-plus-g" aria-hidden="true"></i></a>
		                <a class="btn btn-lg btn-linkedin" href="https://unienrol.com/linkedin/redirect"><i class="fa fa-2x fa-linkedin" aria-hidden="true"></i></a>
		              </div>
                  <div style="margin-top: 16px; margin-bottom: 16px;">
                    <div class="breakline_1">
                    <span class="breakline_2">
                      <span class="breakline_3">
                        <span class="font-medium">OR</span>
                      </span>
                    </span>
                  </div>
                </div>
              <div class="row">
                <div class="col-12 mb-2">
                  <div class="text-input-container">
                    <label for="signup-email" class="font-medium">Email</label>
                    <input v-model="formInputs.email" type="email" name="email" id="signup-email" class="form-control py-4" placeholder="johndoe@email.com" value="" />
                  </div>
                  <span v-if="formErrors['email']" class="error" v-text="formErrors['email'][0]"></span>
                </div>

                <div class="col-12 mb-2">
                  <div class="text-input-container">
                    <label for="signup-name" class="font-medium">Full Name</label>
                    <input v-model="formInputs.name" name="name" id="signup-name" class="form-control py-4" placeholder="John Doe" value="" />
                  </div>
                  <span v-if="formErrors['name']" class="error" v-text="formErrors['name'][0]"></span>
                </div>

                <div class="col-12 mb-2">
                  <div class="text-input-container">
                    <label for="signup-password" class="font-medium">Password</label>
                    <input v-model="formInputs.password" type="password" name="password" id="signup-password" class="form-control py-4" placeholder="************" />
                  </div>
                  <span v-if="formErrors['password']" class="error" v-text="formErrors['password'][0]"></span>
                </div>

                <div class="col-12 mb-2">
                  <div class="text-input-container">
                    <label for="signup-password-confirmation" class="font-medium">Confirm Password</label>
                    <input v-model="formInputs.password_confirmation" type="password" name="password_confirmation" id="signup-password-confirmation" class="form-control py-4" placeholder="************" />
                  </div>
                  <span v-if="formErrors['password_confirmation']" class="error" v-text="formErrors['password_confirmation'][0]"></span>
                </div>

                <div class="col-sm-12 text-center text-primary mt-3">
                  <div class="login-footer" style="display:none;">
                    <a class="login-acct-link text-primary font-medium" @click.self="openLogin">Already have an Unienrol account? Log In</a>
                  </div>
                  <div class="signup-footer">
                    <a class="create-acct-link text-primary font-medium" @click.self="openSignup">Not a member yet? Create your account</a>
                  </div>
                </div>
                <div class="text-center col-sm-12 mt-2" id="tnc-signup">
                  <small class="section-desc font-light">
                      By clicking Sign-up or Login, I agree to Uni Enrol’s <a href="https://unienrol.com/terms">Terms of Service, and Privacy Policy.</a>
                    </small><br><br>
                </div>
                <div class="col-sm-12 text-center">
                  <button class="btn btn-primary font-semibold" type="submit">
                    <i class="fa fa-spinner fa-spin signup-spinner" style="display:none;"></i> SIGN UP
                  </button>
                </div>
              </div>
            </form>
            </div>
            </slot>
          </div>
        </div>
      </div>
    </div>
  </transition>
</script>