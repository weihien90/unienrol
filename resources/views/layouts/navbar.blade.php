<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-white">
    <div class="container" style="position: relative">
        <a class="navbar-brand" href="https://unienrol.com">
            <img src="https://unienrol.com/frontend/images/logos/nav_logo_red.svg"/>
        </a>

        <button class="navbar-toggler clearfix pull-left" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
            aria-expanded="false" aria-label="Toggle Navigation">
            <span><i class="fas fa-angle-down"></i></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <!-- Desktop Navigation Bar -->
            <ul class="navbar-nav d-none d-lg-flex font-bold">
                <li class="nav-item search-item">
                    <a class="nav-link" href="javascript:void(0);" data-searchExpand="nav-link"><i class="fas fa-search mr-1"></i>Search</a>
                    <div class="search-bar nav-searchBar-container">
                        <img class="search-img" src="https://unienrol.com/frontend/images/icons/nav_search_icon.svg" alt="">
                        <input type="text" id="search-term" class="form-control nav-searchBar typeahead" placeholder="Search" data-result="false" />
                        <span class="text-primary" data-closeSearchExpand="close"><i class="fas fa-times"></i></span>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://unienrol.com/scholarships">Courses</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://unienrol.com/university/search">Scholarships</a>
                </li>
                <li class="nav-item {{ Request::is('guide-landing') ? 'active' : '' }}">
                    <a class="nav-link" href="https://unienrol.com/university/search">Guide</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link notification dropdown-toggle" href="#" id="navbarDropdownMenuTools" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Tools <i class="fa fa-chevron-down"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuTools">
                        <a class="dropdown-item position-relative pr-5" href="https://unienrol.com/contact-us">Pathway Check</a>
                        <a class="dropdown-item position-relative pr-5" href="https://unienrol.com/calendar">
                            Compare <span class="badge badge-pill badge-primary">0</span>
                        </a>
                        <a class="dropdown-item position-relative notification pr-5"  href="#" onclick="loginModal();" >My Application</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuTestPrep" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        TestPrep <i class="fa fa-chevron-down"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuTestPrep">
                        <a class="dropdown-item position-relative" href="https://unienrol.com/contact-us">SPM Question Bank</a>
                        <a class="dropdown-item position-relative" href="https://unienrol.com/calendar">Exam Calendar</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://unienrol.com/university/search">ScholarSearch</a>
                </li>
            </ul>

            <!-- Mobile Navigation Bar -->
            <ul class="navbar-nav navbar-mobile d-block d-lg-none">
                <li class="nav-item no-underline">
                    <a class="nav-link" href="#" onclick="loginModal();">
                        <img class="mr-3" src="{{ url('img/header/login_signup.svg') }}" />
                        Login / Signup
                    </a>
                </li>
                
                <li class="nav-item no-underline">
                    <a class="nav-link" href="https://unienrol.com">
                        <img class="mr-3" src="{{ url('img/header/home.svg') }}" />
                        Home
                    </a>
                </li>

                <li class="nav-item no-underline">
                    <a class="nav-link" href="https://unienrol.com">
                        <img class="mr-3" src="{{ url('img/header/courses.svg') }}" />
                        Courses
                    </a>
                </li>

                <li class="nav-item no-underline">
                    <a class="nav-link" href="https://unienrol.com/scholarships">
                        <img class="mr-3" src="{{ url('img/header/scholarships.svg') }}" />
                        Scholarships
                    </a>
                </li>

                <li class="nav-item no-underline">
                    <a class="nav-link" href="https://unienrol.com/university/search">
                        <img class="mr-3" src="{{ url('img/header/guides.svg') }}" />
                        Guides
                    </a>
                </li>

                <li class="nav-item no-underline">
                    <a class="nav-link" data-toggle="collapse" href="#navCollapseMenuTools" role="button" aria-expanded="false">
                        <img class="mr-3" src="{{ url('img/header/tools.svg') }}" />
                        Tools <i class="fa fa-chevron-down"></i>
                    </a>
                </li>

                <div class="collapse collapsible-menu" id="navCollapseMenuTools">
                    <li class="nav-item no-underline">
                        <a class="nav-link" href="https://unienrol.com/university/search">
                            <img class="mr-3" src="{{ url('img/header/pathwaycheck.svg') }}" />
                            Pathway Check
                        </a>
                    </li>

                    <li class="nav-item no-underline">
                        <a class="nav-link" href="https://unienrol.com/compare">
                            <img class="mr-3" src="{{ url('img/header/compare.svg') }}" />
                            Compare <span class="badge badge-pill badge-primary float-right">0</span>
                        </a>
                    </li>

                    <li class="nav-item no-underline">
                        <a class="nav-link"  href="#" onclick="loginModal();" >
                            <img class="mr-3" src="{{ url('img/header/my_application.svg') }}" />
                            My Application
                        </a>
                    </li>
                </div>

                <li class="nav-item no-underline">
                    <a class="nav-link" data-toggle="collapse" href="#navCollapseMenuTestprep" role="button" aria-expanded="false">
                        <img class="mr-3" src="{{ url('img/header/testprep.svg') }}" />
                        Testprep <i class="fa fa-chevron-down"></i>
                    </a>
                </li>

                <div class="collapse collapsible-menu" id="navCollapseMenuTestprep">
                    <li class="nav-item no-underline">
                        <a class="nav-link" href="https://unienrol.com/university/search">
                            <img class="mr-3" src="{{ url('img/header/calender.svg') }}" />
                            Exam Calendar
                        </a>
                    </li>

                    <li class="nav-item no-underline">
                        <a class="nav-link" href="https://unienrol.com/compare">
                            <img class="mr-3" src="{{ url('img/header/spm_questionbank.svg') }}" />
                            SPM Question Bank
                        </a>
                    </li>
                </div>

                <li class="nav-item no-underline">
                    <a class="nav-link" href="https://unienrol.com/article">
                        <img class="mr-3" src="{{ url('img/header/scholarsearch.svg') }}" />
                        Scholar Search
                    </a>
                </li>

                <li class="nav-item no-underline">
                    <a class="nav-link" href="https://unienrol.com/article">
                        <img class="mr-3" src="{{ url('img/header/logout.svg') }}" />
                        Logout
                    </a>
                </li>
            </ul>
        </div>
        <ul class="navbar-nav login-btn">
            <li class="nav-item">
                <a class="nav-link" href="#" onclick="loginModal()">
                    <span>Login</span>
                    <img class="ml-2 d-inline" src="https://unienrol.com/frontend/images/logos/common_assets/login_signup.svg" width="30px"/>
                </a>
            </li>
        </ul>
    </div>
</nav>