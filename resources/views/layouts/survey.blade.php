<div id="surveylink">
    <survey-modal id="survey-modal" v-if="showSurveyModal" @close="showSurveyModal = false"></login-modal>
</div>

<script type="text/x-template" id="survey-modal-template">
    <transition name="modal">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-dialog modal-lg" role="document" @click.stop>
                    <div class="modal-content px-2 px-lg-5 py-4">
                        <div class="modal-header flex-wrap">
                            <h5 class="modal-title font-light">Unienrol Survey</h5>
                            <button type="button" class="close font-medium" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" @click="close">Skip</span>
                            </button>
                            <hr class="w-100 mb-0" />
                        </div>

                        <div class="modal-body px-0">
                            <div class="container-fluid" v-if="step === 0">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="font-medium">Tell us More about you for us to serve you better. <br>Are you student or parent?</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col mx-1 mx-lg-3 py-3 select-type text-center" @click="selectType('student')">
                                        <div class="w-75 w-lg-50 mx-auto mb-3">
                                            <img class="img-fluid" src="{{ url('img/survey/survey_student.svg') }}">
                                        </div>
                                        <p class="font-semibold">I'm a Student / Graduates</p>
                                    </div>
                                    <div class="col mx-1 mx-lg-3 py-3 select-type text-center" @click="selectType('parent')">
                                        <div class="w-75 w-lg-50 50 mx-auto mb-3">
                                            <img class="img-fluid" src="{{ url('img/survey/survey_parent.svg') }}">
                                        </div>
                                        <p class="font-semibold">I'm a Parent</p>
                                    </div>
                                </div>
                            </div>

                            <div class="container-fluid" v-if="step === 1">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="font-medium">Where do you reside?</p>
                                    </div>
                                </div>

                                <div class="row no-gutters">
                                    <div class="col-12 col-lg mr-lg-2 mb-3">
                                        <div class="select-input-container">
                                            <label for="survey-country" class="font-medium">Country</label>
                                            <select v-model="response.country" id="survey-country" class="form-control">
                                                <option value="">Please select a Country</option>
                                                <option value="Malaysia">Malaysia</option>
                                                <option value="Singapore">Singapore</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12 col-lg mb-3">
                                        <div class="select-input-container">
                                            <label for="survey-state" class="font-medium">State</label>
                                            <select v-model="response.state" id="survey-state" class="form-control">
                                                <option value="">Please select a State</option>
                                                <option value="Kuala Lumpur">Kuala Lumpur</option>
                                                <option value="Selangor">Selangor</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="text-input-container">
                                            <label for="survey-city" class="font-medium">City</label>
                                            <input v-model="response.city" id="survey-city" class="form-control py-4" placeholder="Subang Jaya" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="container-fluid mb-5" v-if="step === 2">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="font-medium">Name of the most recent school/institution</p>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col">
                                        <div class="text-input-container">
                                            <label for="survey-school" class="font-medium">School</label>
                                            <input v-model="response.school" id="survey-school" class="form-control py-4" placeholder="SMJK JIT SIN" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="container-fluid mb-5" v-if="step === 3">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="font-medium">Which secondary qualification your child take?</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col" id="select-qualification">
                                        <div class="text-center mt-3">
                                            <span class="section-title font-medium d-block text-center mb-2">Select your secondary Qualification</span>
                                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                <label class="btn btn-primary px-sm-5" v-bind:class="{ active: response.secondaryQualification === 'spm' }">
                                                    <input type="radio" autocomplete="off" id="spm" value="spm" v-model="response.secondaryQualification"> SPM
                                                </label>
                                                <label class="btn btn-primary px-sm-5" v-bind:class="{ active: response.secondaryQualification === 'o-level' }">
                                                    <input type="radio" autocomplete="off" id="o-level" value="o-level" v-model="response.secondaryQualification"> O-Level
                                                </label>
                                                <label class="btn btn-primary px-sm-5" v-bind:class="{ active: response.secondaryQualification === 'others' }">
                                                    <input type="radio" autocomplete="off" id="others" value="others" v-model="response.secondaryQualification"> Others
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="container-fluid mb-lg-5" v-if="(step === 4 && response.secondaryQualification !== 'others')">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="font-medium">Do you have their SPM Result?</p>
                                    </div>
                                    <div class="col-12 d-flex align-items-center" id="have-result">
                                        <input class="mr-2" type="radio" autocomplete="off" value="no" v-model="response.hasResult"> Not Yet
                                        <input class="ml-4 mr-2" type="radio" autocomplete="off" value="yes" v-model="response.hasResult"> Yes
                                    </div>
                                </div>

                                <div class="row mt-1 no-gutters mt-lg-3">
                                    <div v-for="(subjects, grade) in response.results" class="col-6 col-lg-5 pr-1 pr-lg-3 mt-3">
                                        <div class="text-input-container">
                                            <label for="survey-result-@{{ grade }}" class="font-medium">@{{ grade }}</label>
                                            <input v-model="response.results[grade]" id="survey-result-@{{ grade }}" class="form-control py-4" placeholder="Number of Subjects" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="container-fluid mb-lg-5" v-if="(step === 4 && response.secondaryQualification === 'others')">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="font-medium">Please provide details of your qualification</p>
                                    </div>
                                </div>

                                <div class="row mt-1 mt-lg-3">
                                    <div class="col">
                                        <input v-model="response.otherQualification" class="form-control py-4" />
                                    </div>
                                </div>
                            </div>

                            <div class="container-fluid" v-if="step === 5">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="font-medium">Highest qualification obtained for your child</p>
                                    </div>
                                </div>

                                <div class="row mt-2">
                                    <div class="col-lg-10 mt-2">
                                        <div class="select-input-container">
                                            <label for="survey-highest-qualification" class="font-medium">Select Highest Qualification</label>
                                            <select v-model="response.highestQualification" id="survey-highest-qualification" class="form-control">
                                                <option value="">-Please Select-</option>
                                                <option value="secondary">Secondary School</option>
                                                <option value="pre-u">Pre-U</option>
                                                <option value="degree">Bachelor's Degree</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="container-fluid" v-if="step === 6">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="font-medium">If you would like us to contact you via your mobile phone, please leave your phone number below.</p>
                                    </div>
                                </div>

                                <div class="row mt-2 no-gutters">
                                    <div class="col-2 mt-2">
                                        <div class="select-input-container">
                                            <label for="survey-phone-country" class="font-medium d-none d-sm-block">Country</label>
                                            <select v-model="response.phoneCountry" id="survey-phone-country" class="form-control">
                                                <option value="+60">+60</option>
                                                <option value="+65">+65</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col mt-2 ml-lg-2">
                                        <div class="text-input-container" id="survey-phone-container">
                                            <label for="survey-phone" class="font-medium">Phone <span class="d-none d-lg-inline-block"> Number</span></label>
                                            <input v-model="response.phone" id="survey-phone" class="form-control py-4" placeholder="012 3456789" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-2">
                                    <div class="col-12">
                                        <p class="font-medium font-italic">By providing your number, you are providng consent for Uni Enrol to contact you. Your information will not used for any other purposes</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div v-if="step && step < 7" class="modal-footer mt-4">
                            <button type="button" class="btn btn-primary font-semibold px-5 py-2" @click="nextStep" :disabled="!validated">@{{ nextText }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </transition>
</script>