@extends('layouts.app')

@section('custom_css')
    <link href="{{ url('css/privacy/privacy.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<section class="privacy header">
    <div class="container full-height">
        <div class="row pt-2 pb-4 py-md-5">
            <div class="col-12 mt-1">
                <span class="header-title font-semibold d-block">Privacy Statement</span>
                <span class="header-desc d-block pl-md-2">
                    Any and all information collected at this site will be kept strictly confidential and will not be sold, rented, loaned, or otherwise disclosed. 
                    Any information you give to Uni Enrol will be held with the utmost care. A more detailed explanation about how we safeguard your personal information 
                    is described below:
                </span>
            </div>
        </div>
    </div>
</section>

<section class="privacy statement mt-lg-5">
    <div class="container">
        <div class="row my-3">
            <div class="col-12">
                <span class="section-title font-light d-block">Uni Enrol Privacy and Personal Data Protection Policy Statement</span>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col mt-lg-3 mt-md-0">
                <dl>
                    <dt class="font-semibold mb-2">General Information</dt>
                    <dd class="mb-4 mb-md-5">
                        Uni Enrol protects your personal information by complying with the operative legislation concerning personal data protection in Malaysia, 
                        being the Personal Data Protection Act 2010 (“The Act”), which came into effect on 1st January 2013. This policy statement details how Uni 
                        Enrol collects and uses your personal data, and informs you about the personal data protection measures we have put in place. When using our 
                        Service, you consent to the collection, transfer, manipulation, storage, disclosure and other uses of your information only as described in 
                        this Privacy and Personal Data Protection Policy.
                    </dd>

                    <dt class="font-semibold mb-2">The Information We Collect</dt>
                    <dd class="mb-4 mb-md-5">
                        Unless you are registered for online services from Uni Enrol, you remain anonymous when you browse this website. We do not collect personal 
                        information if you are only browsing this website. In order to access our services, or process a transaction between you and a third party, 
                        we may request personal information such as your name, phone number, and email address, and other information that renders you identifiable.
                    </dd>

                    <dt class="font-semibold mb-2">How We Collect Data</dt>
                    <dd class="mb-4 mb-md-5">
                        Uni Enrol collects personal information from you only when you choose to register for and/ or use our services, for example in the process of 
                        using a digital product/service on our website. This personal information encompasses information communicated to us when you voluntarily complete 
                        an application form presented on our website that makes it possible to identify you. By volunteering personal information, you undertake to 
                        communicate accurate information that does not prejudice the interest of any third parties.
                    </dd>

                    <dt class="font-semibold mb-2">Use of Information</dt>
                    <dd class="mb-4 mb-md-5">
                        Upon giving us consent, we may collect and use for the following purposes: <br /><br />
                        Refer you to our newsletter service, and latest products. <br />
                        To enable you to access and use Uni Enrol’s services. <br />
                        To personalize and improve aspects of our overall service to you and our other users, as well as carrying out research such as analyzing market 
                        trends and customer demographics. <br />
                        To communicate with you, in order to verify the personal information provided, or to request any additional information that is vital to 
                        fulfilling the service you have applied for. <br />
                        To facilitate your participation in, and our administration of, any of our activities including contests, promotions, campaigns, polls or surveys <br />
                        To send you information about products and services which we think may be of interest to you. <br />
                        To detect, investigate and prevent any fraudulent, prohibited or illegal activities or misuse of the service
                    </dd>
                </dl>
            </div>
        </div>
    </div>
</section>
@endsection