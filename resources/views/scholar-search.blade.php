@extends('layouts.app')

@section('custom_css')
    <link href="{{ url('css/scholar-search/scholar-search.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('custom_js')
    <script src="{{ url('js/scholar-search/scholar-search.js') }}" />
@endsection

@section('content')
<section class="scholar-search header">
    <div class="owl-carousel" id="header-carousel">
        <div class="item">
            <picture>
                <source srcset="{{ url('img/scholar-search/banner1.jpeg') }}" media="(min-width: 576px)"/>
                <img src="{{ url('img/scholar-search/banner1s.jpeg') }}" />
            </picture>
        </div>
        <div class="item">
            <picture>
                <source srcset="{{ url('img/scholar-search/banner1.jpeg') }}" media="(min-width: 576px)"/>
                <img src="{{ url('img/scholar-search/banner1s.jpeg') }}" />
            </picture>
        </div>
    </div>
</section>

<section class="scholar-search presenting mt-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-xxl-10">
                <div class="media flex-wrap justify-content-center">
                    <div class="media-body order-2 order-md-1">
                        <span class="section-title font-semibold text-primary">Presenting Uni Enrol Scholar Search</span>
                        <div class="section-description font-semibold pt-3 text-justify">
                            <p>Scholar Search was created to match students with suitable university-based and external scholarship based on your academic 
                            excellence, co-curricular achievements and/or socio-ecnonomic backgrounds.</p>
                            <p>Through the insights to potential scholarships plus guidance from our Uni Enrol Counsellors, onthe various education pathways 
                            and institutions, you are now able to choose and pursue your best option in higher education.</p>
                            <p>Our Scholar Search nationwide your has started since December 1028, so book now for your 1-to-1 counselling session!</p>
                        </div>
                        <div class="text-center text-md-right">
                            <button class="btn btn-primary px-5 font-semibold">1 ON 1 COUNSELLING SESSION</button>
                        </div>
                    </div>
                    <div class="ml-md-5 order-1 order-md-2 w-lg-100" >
                        <img src="{{ url('img/scholar-search/scholarsearch_19_logo.jpg') }}" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="scholar-search why mt-5">
    <div class="container full-height">
        <div class="row">
            <div class="col-12 mt-5 text-center">
                <span class="section-title font-semibold">Why Attending ScholarSearch The Best Decision</span>
            </div>
            <div class="col-3 col-lg-2 offset-lg-1 my-5 text-center">
                <div class="why-img mx-auto mb-3"><img class="img-fluid" src="{{ url('img/scholar-search/ic_know_your_study_option.svg') }}"></div>
                <div class="why-img-desc">
                    <span class="font-semibold">Know your Study Option</span>
                </div>
            </div>

            <div class="col-3 col-lg-2 my-5 text-center">
                <div class="why-img mx-auto mb-3"><img class="img-fluid" src="{{ url('img/scholar-search/ic_speak_to_experience_counsellors.svg') }}"></div>
                <div class="why-img-desc">
                    <span class="font-semibold">Speak to Experience Counsellors</span>
                </div>
            </div>

            <div class="col-3 col-lg-2 my-5 text-center">
                <div class="why-img mx-auto mb-3"><img class="img-fluid" src="{{ url('img/scholar-search/ic_get_matched_with_scholarships.svg') }}"></div>
                <div class="why-img-desc">
                    <span class="font-semibold">Get Matched with Scholarships</span>
                </div>
            </div>

            <div class="col-3 col-lg-2 my-5 text-center">
                <div class="why-img mx-auto mb-3"><img class="img-fluid" src="{{ url('img/scholar-search/ic_enrol_with_exclusive_ue_bursaries.svg') }}"></div>
                <div class="why-img-desc">
                    <span class="font-semibold">Enrol with Exclusive UE Bursaries</span>
                </div>
            </div>

            <div class="col-12 col-lg-2 mt-3 mb-5 px-4 border-left align-self-center">
                <button class="btn btn-play text-left font-semibold">Click here to play the full video. <br>
                    <span class="d-xl-block">Choose confidently from your best education option through Uni Enrol<span>
                </button>
            </div>
        </div>
    </div>
</section>

<section class="scholar-search join-us mt-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-xxl-10">
                <div class="col-12 text-center">
                    <span class="section-title font-semibold text-center">Join Us at ScholarSearch 2019</span>
                </div>
                <hr>
                <div class="col-12 text-center mb-4">
                    <span class="section-sub-title text-center">Uni Enrol's Nationwide Visits, Select one to secure your registration now</span>
                </div>

                <div class="row mx-md-4">
                    @for ($i = 0; $i < 16; $i++)
                    <div class="col-md-6 mb-3">
                        <div class="card">
                            <a href="/">
                                <div class="card-body">
                                    <div class="row flex-nowrap">
                                        <div class="mx-3" style="height: 50px">
                                            <img src="{{ url('img/scholar-search/ic_visit_placeholder.svg') }}" />
                                        </div>
                                        <div class="flex-grow-1">
                                            <button class="btn btn-outline-primary pull-right mx-2 font-semibold px-3 py-2">REGISTER</button>
                                            <span class="card-title font-semibold">Nov 3rd - 5th 2018 IPOH, PERAK</span> <br>
                                            <span class="card-desc font-light">AEON MALL KLEBANG, PERAK</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endfor
                </div>

            </div>
        </div>
    </div>
</section>

<section class="scholar-search impact-map mt-5 py-3">
    <div class="container">
        <div class="col-lg-10 offset-lg-1">
            <div class="col-12 text-center">
                <span class="section-title font-semibold text-center">UNI ENROl IMPACT MAP</span>
            </div>
            <div class="col-12 text-center mb-4">
                <span class="section-sub-title text-center font-light">
                    Since the incorporation of Uni Enrol in 2017, we have engaged and counselled over 2000 students and helped many of them secure 
                    scholarships and financial aid. Our impact and footprint continue to grow rapidly!
                </span>
            </div>
            <div class="row">
                <div class="col-6 col-md-3 my-5 text-center">
                    <div class="impact-img mb-3"><img src="https://unienrol.com/frontend/images/Scholarship/Landing_page_icons/150_students.svg"></div>
                    <div class="impact-numbers font-bold"><span>34</span></div>
                    <div class="impact-title font-light"><span>Students</span></div>
                    <div class="impact-desc font-medium">
                        <span>Awarded with Full Scholarships</span>
                    </div>
                </div>

                <div class="col-6 col-md-3 my-5 text-center">
                    <div class="impact-img mb-3"><img src="https://unienrol.com/frontend/images/Scholarship/Landing_page_icons/150_students.svg"></div>
                    <div class="impact-numbers font-bold"><span>RM32,000</span></div>
                    <div class="impact-title font-light"><span>Uni Enrol Bursary</span></div>
                    <div class="impact-desc font-medium">
                        <span>allocated to students as part of our own sponsorship</span>
                    </div>
                </div>

                <div class="col-6 col-md-3 my-5 text-center">
                    <div class="impact-img mb-3"><img src="https://unienrol.com/frontend/images/Scholarship/Landing_page_icons/150_students.svg"></div>
                    <div class="impact-numbers font-bold"><span>RM 4 Million</span></div>
                    <div class="impact-title font-light"><span>Total Savings</span></div>
                    <div class="impact-desc font-medium">
                        <span>for 150 students we enrolled</span>
                    </div>
                </div>

                <div class="col-6 col-md-3 my-5 text-center">
                    <div class="impact-img mb-3"><img src="https://unienrol.com/frontend/images/Scholarship/Landing_page_icons/150_students.svg"></div>
                    <div class="impact-numbers font-bold"><span>RM 25,000</span></div>
                    <div class="impact-title font-light"><span>Average Savings</span></div>
                    <div class="impact-desc font-medium">
                        <span>per students</span>
                    </div>
                </div>
            </div>

            <div class="col-12 text-center mb-3 d-none d-md-block">
                <img class="align-self-center img-fluid" src="{{ url('img/scholar-search/bg_malaysia_state_map.svg') }}" />
            </div>

            <div class="col-12 text-center mb-4 d-none d-md-block">
                <span class="section-description text-center">
                    Uni Enrol has helped over 230 students enrol at top institutions with an average scholarship worth over MYR80,000
                </span>
            </div>

            <div class="row d-md-none">
            @for ($i = 0; $i < 12; $i++)
                <div class="font-semibold col-6">
                    <div class="p-2 my-1 impact-map-box ">
                        <span class="box-title d-block mb-2">Kelantan</span>
                        72 Online Registration <br>
                        24 Walk in Registration
                    </div>
                </div>
            @endfor
            </div>
        </div>
    </div>
</section>

<section class="scholar-search sponsors mt-5">
    <div class="container">
        <div class="col-lg-10 offset-lg-1 mb-5">
            <div class="col-12 mt-3">
                <span class="section-title font-semibold">Sponsors & Partners</span>
            </div>
            <div class="col-12 mb-4">
                <span class="section-description">A big thank you for the supports and partners all this while to help ScholarSearch a success since 2017 Jun</span>
            </div>
            <div class="col-12 mt-3">
                <hr>
            </div>
            <div class="col-12 mt-1 mb-4 text-center">
                <span class="section-description">Atlantic Sponsors</span>
            </div>
            <div class="row align-items-center mx-xl-5">
                <div class="col-6 col-md-3 col-xl-2 mt-1 text-center img-fluid">
                    <img src="https://via.placeholder.com/104x52" />
                </div>
                <div class="col-6 col-md-3 col-xl-2 mt-1 text-center img-fluid">
                    <img src="https://via.placeholder.com/130x65" />
                </div>
                <div class="col-6 col-md-3 col-xl-2 mt-1 text-center img-fluid">
                    <img src="https://via.placeholder.com/104x52" />
                </div>
                <div class="col-6 col-md-3 col-xl-2 mt-1 text-center img-fluid">
                    <img src="https://via.placeholder.com/130x65" />
                </div>
                <div class="col-6 col-md-3 col-xl-2 mt-1 text-center img-fluid">
                    <img src="https://via.placeholder.com/104x52" />
                </div>
                <div class="col-6 col-md-3 col-xl-2 mt-1 text-center img-fluid">
                    <img src="https://via.placeholder.com/130x65" />
                </div>
            </div>
            <div class="col-12 mt-3">
                <hr>
            </div>
            <div class="col-12 mt-1 mb-4 text-center">
                <span class="section-description">Indian Sponsors</span>
            </div>
            <div class="row align-items-center mx-xl-5">
                <div class="col-6 col-md-3 col-xl-2 mt-1 text-center img-fluid">
                    <img src="https://via.placeholder.com/104x52" />
                </div>
                <div class="col-6 col-md-3 col-xl-2 mt-1 text-center img-fluid">
                    <img src="https://via.placeholder.com/130x65" />
                </div>
                <div class="col-6 col-md-3 col-xl-2 mt-1 text-center img-fluid">
                    <img src="https://via.placeholder.com/104x52" />
                </div>
                <div class="col-6 col-md-3 col-xl-2 mt-1 text-center img-fluid">
                    <img src="https://via.placeholder.com/130x65" />
                </div>
                <div class="col-6 col-md-3 col-xl-2 mt-1 text-center img-fluid">
                    <img src="https://via.placeholder.com/104x52" />
                </div>
                <div class="col-6 col-md-3 col-xl-2 mt-1 text-center img-fluid">
                    <img src="https://via.placeholder.com/130x65" />
                </div>
            </div>
            <div class="col-12 mt-3">
                <hr>
            </div>
            <div class="col-12 mt-1 mb-4 text-center">
                <span class="section-description">Artic Sponsors</span>
            </div>
            <div class="row align-items-center mx-xl-5">
                <div class="col-6 col-md-3 col-xl-2 offset-xl-4 mt-1 text-center img-fluid">
                    <img src="https://via.placeholder.com/104x52" />
                </div>
                <div class="col-6 col-md-3 col-xl-2 mt-1 text-center img-fluid">
                    <img src="https://via.placeholder.com/130x65" />
                </div>
            </div>
        </div>
    </div>
</section>

<section class="scholar-search faq mt-5">
    <div class="container">
        <div class="row mb-5">
            <div class="col-lg-10 offset-lg-1">
                <div class="col-12 mt-3">
                    <span class="section-title font-semibold">Frequenty Asked Question</span>
                </div>

                <div class="owl-carousel p-5" id="faq-carousel">
                    <div class="item">
                        <h4 class="font-medium mb-3">Lorem ipsum</h4>
                        <span class="item-desc font-light">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure 
                            dolor in reprehenderit in voluptate
                        </span>
                    </div>
                    <div class="item">
                        <h4 class="font-medium mb-3">Lorem ipsum</h4>
                        <span class="item-desc font-light">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure 
                            dolor in reprehenderit in voluptate
                        </span>
                    </div>
                    <div class="item">
                        <h4 class="font-medium mb-3">Lorem ipsum</h4>
                        <span class="item-desc font-light">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure 
                            dolor in reprehenderit in voluptate
                        </span>
                    </div>
                    <div class="item">
                        <h4 class="font-medium mb-3">Lorem ipsum</h4>
                        <span class="item-desc font-light">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure 
                            dolor in reprehenderit in voluptate
                        </span>
                    </div>
                    <div class="item">
                        <h4 class="font-medium mb-3">Lorem ipsum</h4>
                        <span class="item-desc font-light">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure 
                            dolor in reprehenderit in voluptate
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="scholar-search sign-up mt-4 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <span class="section-description font-light mb-5 px-2 text-center">
                Get a Uni Enrol account today & keep yourself updated on the best education and scholarship options!
            </span>
            <div class="col-12 text-center">
                <button class="btn btn-primary px-5 font-semibold">SIGN UP NOW, IT'S FREE!</button>
            </div>
        </div>
    </div>
</section>
@endsection