@extends('layouts.app')

@section('custom_css')
    <link href="{{ url('css/server-error/server-error.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<section class="server-error">
    <div class="container">
        <div class="row justify-content-center my-5">
            <div class="col-12 text-center mb-4">
                <span class="section-title font-light">500 INTERNAL SERVER ERROR</span>
            </div>
            <div class="col-7 col-md-5 text-center mb-4">
                <img src="{{ url('img/server-error/ic_page_error_bg.svg') }}" />
            </div>
            <div class="col-12 text-center mb-2">
                <span class="section-title font-light">Oops, Seems like something has broken</span>
            </div>
            <div class="col-12 text-center mb-3">
                <span class="section-description font-medium">We'll make sure to get it fixed as soon as possible</span>
            </div>
            <div class="col-12 text-center">
                <button class="btn btn-primary px-5 py-2 font-semibold mx-2 mb-1">BACK TO HOME PAGE</button>
                <button class="btn btn-primary px-5 py-2 font-semibold mx-2 mb-1">GET IN TOUCH WITH US</button>
            </div>
        </div>
    </div>
</section>
@endsection