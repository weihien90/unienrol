@extends('layouts.app')

@section('custom_css')
    <link href="{{ url('css/testprep-landing/testprep-landing.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<section class="testprep landing">
    <div class="container-fluid py-2 py-md-5">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-9">
                <span class="header-title font-semibold d-block">Free Exam Preparation for Uni Enrol Members!</span>
                <span class="header-desc d-block">“There are no secrets to success. It is the result of preparation, hard work, and learning from failure.”</span>
                <span class="header-desc d-block font-medium" id="quote-from">- Colin Powell</span>

                <div class="row px-md-4 pb-5">
                    <div class="col-md-8 left-border mt-4 mt-md-5 order-1">
                        <span class="section-title font-semibold d-block">Introducing Test Prep by Uni Enrol</span>
                        <span class="section-desc d-block mt-3">
                            Test Prep is our way of giving back to the community to help you and your peers prepare and score well in your exams so that you can 
                            qualify for awesome scholarships when you use our website to find your dream university! <br><br>
                            At Uni Enrol, we believe that students should be empowered with the necessary information to make a well-informed decision. Our website 
                            helps you to search for the best university and course that suits your passion and preference!
                        </span>
                    </div>

                    <div class="col-md-4 left-border mt-5 mt-md-5 order-2">
                        <span class="section-title font-semibold d-block">The countdown until your SPM exam has begun!</span>
                        <span class="section-desc d-block mt-3">
                            We know you need as much preparation as you can. We’ve prepared mock exam papers for you to practice and evaluate how far you are with your 
                            studying progress. Click on the sign up button below, or if you’re an existing user, login to access them.
                        </span>
                    </div>

                    <div class="col-12 order-0 order-md-3 mt-3 mt-md-5">
                        <button class="btn btn-primary font-semibold px-4 px-md-5 py-2 mt-3 mt-md-0 mr-2">LOGIN</button>
                        <button class="btn btn-primary font-semibold px-4 px-md-5 py-2 mt-3 mt-md-0">SIGN UP FOR FREE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection