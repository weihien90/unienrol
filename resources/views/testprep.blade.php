@extends('layouts.app')

@section('custom_css')
    <link href="{{ url('css/testprep/testprep.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<section class="testprep header">
    <div class="container full-height">
        <div class="row pt-2 pb-4 py-md-5">
            <div class="col-12 mt-1">
                <span class="header-title font-semibold d-block">Free Exam Preparation for Uni Enrol Members!</span>
                <span class="header-desc d-block">“There are no secrets to success. It is the result of preparation, hard work, and learning from failure.”</span>
                <span class="header-desc d-block font-medium" id="quote-from"><u>- Colin Powell</u></span>
            </div>
        </div>
    </div>
</section>

<section class="testprep categories my-3">
    <div class="container">
        <div class="row">
            <div class="col-12 mt-3">
                <span class="section-title font-light">Our Question Bank</span>
                <div class="float-right mt-2 font-medium" id="clear-filter">
                    <span class="mr-1">x</span> Clear Filter
                </div>
                <hr />
            </div>
        </div>

        <div class="row">
            <div class="col-12">
            @for ($i = 0; $i < 15; $i++)
                <button class="btn px-3 px-md-4 mr-2 mb-2 mb-md-3 py-1 font-medium btn-categories">
                    {{ str_limit('Additional Mathematics', rand(10, 21), '') }}
                </button>
            @endfor
            </div>

            <div class="col-12 my-4 text-center" id="best-viewed">
                <i>* Best viewed on a screen larger than 768 pixels (equivalent to a tablet or larger screens).</i>
            </div>
        </div>

        <div class="row no-gutters mb-5">
            @for ($i = 0; $i < 24; $i++)
            <div class="col-6 col-md-3 col-xl-2 mt-3 px-1 px-lg-2">
                <img class="img-fluid w-100" src="https://via.placeholder.com/195x259">
            </div>
            @endfor
        </div>
    </div>
</section>
@endsection