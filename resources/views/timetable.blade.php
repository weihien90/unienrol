@extends('layouts.app')

@section('custom_css')
    <link href="{{ url('css/calendar-tools/calendar-tools.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('custom_js')
    <script src="{{ url('js/calendar-tools/calendar-tools.js') }}"></script>
@endsection

@section('content')
<section class="calendar-tools header">
    <div class="container full-height">
        <div class="row pt-2 pb-4 py-md-5">
            <div class="col-12 mt-1">
                <span class="header-title font-semibold d-block">SPM TIME TABLE</span>
                <span class="header-desc d-block">Stay aware and ahead on your Exam Schedules</span>
                <hr />
                <span class="header-desc d-lg-inline-block d-block">Check out Uni Enrol exclusive SPM test prep online tool today</span>
                <button class="btn btn-primary font-semibold px-4 px-md-5 py-2 mt-2 float-lg-right">TEST PREP</button>
            </div>
        </div>
    </div>
</section>

<section class="calendar-tools exam-schedules mb-5">
    <div class="container">
        <div class="row">
            <div class="col-12 mt-3">
                <span class="section-title font-light">Jump to Date</span>
                <hr />
            </div>
        </div>

        <div class="row no-gutters">
            @for ($i=0; $i<17; $i++)
            <div class="col-6 col-sm-4 col-lg-2 pr-3 mb-3">
                <div class="schedule-box d-flex flex-column justify-content-center align-items-center py-3">
                    <span class="exam-week font-light">Week {{ floor($i / 4) + 1 }}</span>
                    <span class="exam-date font-semibold" data-date="{{ $i+1 }}-11">{{ $i+1 }} Nov</span>
                </div>
            </div>
            @endfor
        </div>
    </div>
</section>

<section class="calendar-tools filters">
    <div class="container">
        <div class="row">
            <div class="col-12 mt-3">
                <span class="section-title font-light">Filter by Category</span>
                <div class="float-right mt-2 font-medium" id="clear-filter" @click="changeType('')">
                    <span class="mr-1">x</span> Clear Filter
                </div>
                <hr />
            </div>
        </div>

        <div class="row" id="filter-buttons">
            <div class="col-12 mt-3">
                <button type="button" @click="changeType('compulsory')" :class="{ active: type === 'compulsory'}" class="btn font-medium mb-2 btn-filters btn-outline-success">Compulsory</button>
                <button type="button" @click="changeType('language')" :class="{ active: type === 'language'}" class="btn font-medium mb-2 btn-filters btn-outline-warning">Language & literature</button>
                <button type="button" @click="changeType('social')" :class="{ active: type === 'social'}" class="btn font-medium mb-2 btn-filters btn-outline-secondary">Social Science & Religion</button>
                <button type="button" @click="changeType('arts')" :class="{ active: type === 'arts'}" class="btn font-medium mb-2 btn-filters btn-outline-info">Arts & Health</button>
                <button type="button" @click="changeType('science')" :class="{ active: type === 'science'}" class="btn font-medium mb-2 btn-filters btn-outline-primary">Science & Mathematics</button>
                <button type="button" @click="changeType('technical')" :class="{ active: type === 'technical'}" class="btn font-medium mb-2 btn-filters btn-outline-success">Technical & Vocational</button>
            </div>
        </div>

        <div class="row" id="calendar-card-container">
            <div class="col-12">
                @for ($i=0; $i<12; $i++)
                <div class="row flex-nowrap my-4 date-{{ $i+1 }}-11" v-if="!type || type === 'compulsory'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        {{ $i+1 }}/11<br />Tue
                    </div>

                    <div class="w-100">
                        <div class="calendar-card success p-3">
                            <span class="card-date font-semibold">8:00 am - 10:15 am</span><br />
                            <span class="card-desc">Bahasa Melayu - Kertas 1</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor

                @for ($i=0; $i<6; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'language'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        13/11<br />Tue
                    </div>

                    <div class="w-100">
                        <div class="calendar-card warning p-3">
                            <span class="card-date font-semibold">10:00 am - 11:15 am</span><br />
                            <span class="card-desc">English Literature - Paper 1</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor

                @for ($i=0; $i<6; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'social'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        13/11<br />Tue
                    </div>

                    <div class="w-100">
                        <div class="calendar-card secondary p-3">
                            <span class="card-date font-semibold">10:00 am - 11:15 am</span><br />
                            <span class="card-desc">English Literature - Paper 1</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor

                @for ($i=0; $i<6; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'arts'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        13/11<br />Tue
                    </div>

                    <div class="w-100">
                        <div class="calendar-card info p-3">
                            <span class="card-date font-semibold">10:00 am - 11:15 am</span><br />
                            <span class="card-desc">English Literature - Paper 1</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor

                @for ($i=0; $i<6; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'arts'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        13/11<br />Tue
                    </div>

                    <div class="w-100">
                        <div class="calendar-card info p-3">
                            <span class="card-date font-semibold">10:00 am - 11:15 am</span><br />
                            <span class="card-desc">English Literature - Paper 1</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor

                @for ($i=0; $i<6; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'science'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        13/11<br />Tue
                    </div>

                    <div class="w-100">
                        <div class="calendar-card primary p-3">
                            <span class="card-date font-semibold">10:00 am - 11:15 am</span><br />
                            <span class="card-desc">English Literature - Paper 1</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor

                @for ($i=0; $i<6; $i++)
                <div class="row flex-nowrap my-4" v-if="!type || type === 'technical'">
                    <div class="d-flex align-items-center flex-shrink-1 px-3 px-lg-5 list-month font-light">
                        13/11<br />Tue
                    </div>

                    <div class="w-100">
                        <div class="calendar-card success p-3">
                            <span class="card-date font-semibold">10:00 am - 11:15 am</span><br />
                            <span class="card-desc">English Literature - Paper 1</span>
                        </div>
                    </div>

                    <div class="col">
                    </div>
                </div>
                @endfor
            </div>
        </div>
    </div>
</section>

<button class="btn btn-primary py-1" id="go-top">
    <i class="fa fa-angle-double-up"></i>
</button>
@endsection